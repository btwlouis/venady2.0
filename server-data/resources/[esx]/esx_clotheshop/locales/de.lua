Locales['de'] = {
  ['valid_this_purchase'] = 'einkauf bestätigen?',
  ['yes'] = 'ja',
  ['no'] = 'nein',
  ['not_enough_money'] = 'du hast nicht genug Geld',
  ['press_menu'] = 'drücke ~INPUT_CONTEXT~ um das Menü zu öffnen',
  ['clothes'] = 'kleidung',
  ['you_paid'] = 'du bezahlst ~g~$%s~s~',
  ['save_in_dressing'] = 'Möchtest du dein Outfit in deinem Haus speichern?',
  ['name_outfit'] = 'name des outfits?',
  ['saved_outfit'] = 'Das Outfit wurde gespeichert!',
}