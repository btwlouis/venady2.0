var tableauQuestion = [
	{
		question: "Wenn Sie 80 km / h fahren und sich einem Wohngebiet nähern, müssen Sie:",
		propositionA: "Sie beschleunigen",
		propositionB: "Sie behalten Ihre Geschwindigkeit, wenn Sie nicht an anderen Fahrzeugen vorbeifahren",
		propositionC: "Du wirst langsamer",
		propositionD: "Du behälst deine Geschwindigkeit",
		reponse: "C"
	},

	{
		question: "Wenn Sie an einer Ampel rechts abbiegen, aber einen Fußgängerüberweg sehen, was tun Sie dann?:",
		propositionA: "Sie passieren den Fußgänger",
		propositionB: "Sie überprüfen, ob sich keine anderen Fahrzeuge in der Nähe befinden",
		propositionC: "Sie warten, bis der Fußgänger überquert hat",
		propositionD: "Sie schießen auf den Fußgänger und fahren weiter",
		reponse: "C"
	},

	{
		question: "Ohne vorherige Angabe beträgt die Geschwindigkeit in einem Wohngebiet: __ km / h",
		propositionA: "30 km/h",
		propositionB: "50 km/h",
		propositionC: "40 km/h",
		propositionD: "60 km/h",
		reponse: "B"
	},

	{
		question: "Vor jedem Spurwechsel müssen Sie:",
		propositionA: "Überprüfen Sie Ihre Spiegel",
		propositionB: "Überprüfen Sie Ihre toten Winkel",
		propositionC: "Signalisieren Sie Ihre Absichten",
		propositionD: "Alles das oben Genannte",
		reponse: "D"
	},

	{
		question: "Welcher Blutalkoholspiegel wird als Fahren während des Rauschens eingestuft?",
		propositionA: "0.05%",
		propositionB: "0.18%",
		propositionC: "0.08%",
		propositionD: "0.06%",
		reponse: "C"
	},

	{
		question: "Wann können Sie an einer Ampel weiterfahren?",
		propositionA: "Wenn es Grün ist",
		propositionB: "Wenn sich niemand an der Kreuzung befindet",
		propositionC: "Sie befinden sich in einer Schulzone",
		propositionD: "Wenn es grün und / oder rot ist und Sie rechts abbiegen",
		reponse: "D"
	},

	{
		question: "Ein Fußgänger hat kein Kreuzsignal. Was machen Sie?",
		propositionA: "Du lässt sie passieren"
		propositionB: "Sie beobachten, bevor Sie fortfahren",
		propositionC: "Sie winken, um ihnen zu sagen, sie sollen überqueren",
		propositionD: "Sie fahren fort, weil Ihre Ampel grün ist",
		reponse: "D"
	},

	{
		question: "Was ist erlaubt, wenn Sie an einem anderen Fahrzeug vorbeifahren?",
		propositionA: "Sie folgen ihm genau, um ihn schneller zu passieren",
		propositionB: "Sie passieren es, ohne die Fahrbahn zu verlassen",
		propositionC: "Sie fahren auf der gegenüberliegenden Straßenseite vorbei",
		propositionD: "Sie überschreiten das Tempolimit, um sie zu passieren",
		reponse: "C"
	},

	{
		question: "Sie fahren auf einer Autobahn, die eine Höchstgeschwindigkeit von 120 km / h anzeigt. Die meisten Verkehrsteilnehmer fahren jedoch mit 125 km / h, daher sollten Sie nicht schneller fahren als",
		propositionA: "120 km/h",
		propositionB: "125 km/h",
		propositionC: "130 km/h",
		propositionD: "110 km/h",
		reponse: "A"
	},

	{
		question: "Wenn Sie von einem anderen Fahrzeug überholt werden, ist es wichtig, NICHT:",
		propositionA: "Langsamer",
		propositionB: "Überprüfen Sie Ihre Spiegel",
		propositionC: "Beobachten Sie andere Fahrer",
		propositionD: "Erhöhen Sie Ihre Geschwindigkeit",
		reponse: "D"
	},
]
