Locales['en'] = {
  ['prompt_wash'] = 'Drücke ~INPUT_CONTEXT~ um dein Fahrzeug zu waschen',
  ['prompt_wash_paid'] = 'Drücke ~INPUT_CONTEXT~ um das Fahrzeug zu waschen für ~g~$%s~s~',
  ['wash_failed'] = 'Du kannst dir die Reinigung nicht leisten',
  ['wash_failed_clean'] = 'Dein Fahrzeug ist sauber genug.',
  ['wash_successful'] = 'Dein Fahrzeug wurde gereinigt',
  ['wash_successful_paid'] = 'Dein Fahrzeug wurde gereinigt für ~g~$%s~s~',
  ['blip_carwash'] = 'Waschanlage',
}
