Locales['de'] = {
  ['shop'] = 'Geschäft',
  ['shops'] = 'Geschäfte',
  ['press_menu'] = 'drücke ~INPUT_CONTEXT~ um auf das Geschäft zuzugreifen.',
  ['shop_item'] = '$%s',
  ['bought'] = 'Du hast gekauft ~y~%sx~s~ ~b~%s~s~ für ~r~$%s~s~',
  ['not_enough'] = 'du ~r~hast nicht~s~ genug geld: %s',
  ['player_cannot_hold'] = 'Du hast ~r~nicht~s~ genug ~y~platz~s~ in deinem Inventar!',
  ['shop_confirm'] = 'Kaufe %sx %s für $%s?',
  ['no'] = 'Nein',
  ['yes'] = 'Ja',
}
