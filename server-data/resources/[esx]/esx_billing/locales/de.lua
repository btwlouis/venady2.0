Locales['de'] = {
  ['invoices'] = 'Rechnungen',
  ['invoices_item'] = '$%s',
  ['received_invoice'] = 'Du hast eine Rechnung ~r~erhalten~s~',
  ['paid_invoice'] = 'Du ~g~bezahlst~s~ eine Rechnung von ~r~$%s~s~',
  ['no_invoices'] = 'Du hast aktuell ~g~keine Rechnung~s~ die du bezahlen musst!',
  ['received_payment'] = 'du ~g~erhältst~s~ eine Zahlung von ~r~$%s~s~',
  ['player_not_online'] = 'der Spieler ist nicht online',
  ['no_money'] = '~r~Du hast nicht genug Geld um die Rechnung zu bezahlen!~s~',
  ['target_no_money'] = 'Der Spieler ~r~hat nicht~s~ genug Geld um die Rechnung zu bezahlen!',
  ['keymap_showbills'] = 'Rechnungmenü öffnen',
}
