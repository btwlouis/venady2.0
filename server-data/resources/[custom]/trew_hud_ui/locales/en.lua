-- English
Locales['en'] = {
	['you_are_on_location'] = 'You are on <strong>%s</strong>',

	['date_format'] = {
		default = 'Today is <strong>%s of %s, %s</strong>',
		simple = 'Today is <strong>%s, %s</strong>',
		simpleWithHours = 'It is <strong>%s</strong>, <strong>%s, %s</strong>',
		withWeekday = 'Today is <strong>%s, %s of %s, %s</strong>',
		withHours = 'It is <strong>%s</strong>, <strong>%s of %s, %s</strong>',
		withWeekdayAndHours = 'It is <strong>%s</strong>, <strong>%s, %s of %s, %s</strong>'
	},

	['weekDay_0'] = 'Sonntag',
	['weekDay_1'] = 'Montag',
	['weekDay_2'] = 'Dienstag',
	['weekDay_3'] = 'Mittwoch',
	['weekDay_4'] = 'Donnerstag',
	['weekDay_5'] = 'Freitag',
	['weekDay_6'] = 'Samstag',

	['day_1'] = '1',
	['day_2'] = '2',
	['day_3'] = '3',
	['day_4'] = '4',
	['day_5'] = '5',
	['day_6'] = '6',
	['day_7'] = '7',
	['day_8'] = '8',
	['day_9'] = '9',

	['day_10'] = '10',
	['day_11'] = '11',
	['day_12'] = '12',
	['day_13'] = '13',
	['day_14'] = '14',
	['day_15'] = '15',
	['day_16'] = '16',
	['day_17'] = '17',
	['day_18'] = '18',
	['day_19'] = '19',

	['day_20'] = '20',
	['day_21'] = '21',
	['day_22'] = '22',
	['day_23'] = '23',
	['day_24'] = '24',
	['day_25'] = '25',
	['day_26'] = '26',
	['day_27'] = '27',
	['day_28'] = '28',
	['day_29'] = '29',

	['day_30'] = '30',
	['day_31'] = '31',

	['month_0'] = 'Januar',
	['month_1'] = 'Februar',
	['month_2'] = 'März',
	['month_3'] = 'April',
	['month_4'] = 'Mai',
	['month_5'] = 'Juni',
	['month_6'] = 'Juli',
	['month_7'] = 'August',
	['month_8'] = 'September',
	['month_9'] = 'Oktober',
	['month_10'] = 'November',
	['month_11'] = 'Dezember',

	['toggleui'] = 'Enables/Disables parts of the HUD',

}