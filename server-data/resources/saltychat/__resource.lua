resource_manifest_version "44febabe-d386-4d18-afbe-5e627f4af937"

ui_page "NUI/SaltyWebSocket.html"

client_scripts {
    "SaltyClient/bin/Debug/SaltyClient.net.dll"
}

server_scripts {
    "SaltyServer/bin/Debug/netstandard2.0/SaltyServer.net.dll"
}

files {
    "NUI/SaltyWebSocket.html",
    "Newtonsoft.Json.dll",
}

exports {
    "EstablishCall",
    "EndCall",

    "SetPlayerRadioSpeaker",
    "SetPlayerRadioChannel",
    "RemovePlayerRadioChannel",
    "SetRadioTowers"
}

VoiceEnabled "true"
ServerUniqueIdentifier "O3C2KS3lh8T7KUwWIi6bKNi3ZnQ="
RequiredUpdateBranch "Stable"
MinimumPluginVersion "0.5.0"
SoundPack "default"
IngameChannelId "3"
IngameChannelPassword "5V88FWWME615"
SwissChannelIds "1"
