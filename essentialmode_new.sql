-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 18. Feb 2020 um 13:57
-- Server-Version: 10.3.22-MariaDB-0ubuntu0.19.10.1
-- PHP-Version: 7.3.11-0ubuntu0.19.10.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `essentialmode`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `addon_account`
--

CREATE TABLE `addon_account` (
  `name` varchar(60) NOT NULL,
  `label` varchar(100) NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `addon_account`
--

INSERT INTO `addon_account` (`name`, `label`, `shared`) VALUES
('caution', 'Caution', 0),
('property_black_money', 'Argent Sale Propriété', 0),
('society_ambulance', 'Ambulance', 1),
('society_cardealer', 'Händler', 1),
('society_police', 'Polizei', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `addon_account_data`
--

CREATE TABLE `addon_account_data` (
  `id` int(11) NOT NULL,
  `account_name` varchar(100) DEFAULT NULL,
  `money` int(11) NOT NULL,
  `owner` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `addon_account_data`
--

INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
(1, 'society_cardealer', 227700, NULL),
(2, 'society_police', 1000000, NULL),
(3, 'society_ambulance', 0, NULL),
(4, 'property_black_money', 0, 'steam:110000114cf3e89'),
(5, 'property_black_money', 0, 'steam:11000011300960a'),
(6, 'caution', 0, 'steam:110000114cf3e89'),
(7, 'caution', 0, 'steam:11000011300960a'),
(8, 'caution', 0, 'steam:11000010acc835e'),
(9, 'property_black_money', 0, 'steam:11000010acc835e'),
(10, 'caution', 0, 'steam:11000013f9a232d'),
(11, 'property_black_money', 0, 'steam:11000013f9a232d'),
(12, 'caution', 0, 'steam:11000011b642819'),
(13, 'property_black_money', 0, 'steam:11000011b642819'),
(14, 'caution', 0, 'steam:1100001102420a8'),
(15, 'property_black_money', 0, 'steam:1100001102420a8'),
(16, 'caution', 0, 'steam:11000013f5799ba'),
(17, 'property_black_money', 0, 'steam:11000013f5799ba'),
(18, 'caution', 0, 'steam:11000011a0c3cab'),
(19, 'property_black_money', 0, 'steam:11000011a0c3cab'),
(20, 'caution', 0, 'steam:110000136dd07fc'),
(21, 'property_black_money', 0, 'steam:110000136dd07fc'),
(22, 'caution', 0, 'steam:1100001195c1d46'),
(23, 'property_black_money', 0, 'steam:1100001195c1d46');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `addon_inventory`
--

CREATE TABLE `addon_inventory` (
  `name` varchar(60) NOT NULL,
  `label` varchar(100) NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `addon_inventory`
--

INSERT INTO `addon_inventory` (`name`, `label`, `shared`) VALUES
('property', 'Propriété', 0),
('society_ambulance', 'Ambulance', 1),
('society_cardealer', 'Händler', 1),
('society_police', 'Polizei', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `addon_inventory_items`
--

CREATE TABLE `addon_inventory_items` (
  `id` int(11) NOT NULL,
  `inventory_name` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `billing`
--

CREATE TABLE `billing` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `sender` varchar(255) NOT NULL,
  `target_type` varchar(50) NOT NULL,
  `target` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `cardealer_vehicles`
--

CREATE TABLE `cardealer_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `characters`
--

CREATE TABLE `characters` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `dateofbirth` varchar(255) NOT NULL,
  `sex` varchar(1) NOT NULL DEFAULT 'M',
  `height` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `characters`
--

INSERT INTO `characters` (`id`, `identifier`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`) VALUES
(4, 'steam:11000011300960a', 'Asdef', 'Aysdfc', '20000000', 'm', '200'),
(5, 'steam:11000011300960a', 'Lukas', 'Jac', '2000-02-27', 'm', '200'),
(9, 'steam:110000114cf3e89', 'Test', 'Test', '20000402', 'm', '200'),
(10, 'steam:11000011300960a', 'Clarissa', 'Jacko', '2000-02-27', 'm', '200'),
(11, 'steam:110000114cf3e89', 'Louis', 'Jackson', '20000402', 'm', '200'),
(12, 'steam:11000011300960a', 'Lukas', 'Jac', '2000-02-27', 'm', '200'),
(13, 'steam:11000010acc835e', 'Aasd', 'AADAD', '445245654', 'm', '200'),
(14, 'steam:11000013f9a232d', 'Mr', 'Hitman', '13.09.1990', 'm', '200'),
(15, 'steam:110000114cf3e89', 'Louis', 'Jackson', '20000402', 'm', '200'),
(16, 'steam:11000011b642819', 'Louis', 'Black', '04.05.1997', 'm', '187'),
(17, 'steam:1100001102420a8', 'Kevin', 'Hilton', '20060113', 'm', '180'),
(18, 'steam:1100001102420a8', 'Kevin', 'Hilton', '20060113', 'm', '180'),
(19, 'steam:11000013f5799ba', 'John', 'Englisch', '1990.10.10', 'm', '180'),
(20, 'steam:11000011a0c3cab', 'Sebastian', 'New', '2002-09-17', 'm', '189'),
(21, 'steam:110000136dd07fc', 'Dennis', 'Adams', '20.09.2000', 'm', '190'),
(22, 'steam:110000136dd07fc', 'Ddfghzj', 'Sfsafsadfda', '11.11.11', 'm', '190'),
(23, 'steam:1100001195c1d46', 'Kalle', 'Kuschinsky', '25.5.1990', 'm', '186');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `datastore`
--

CREATE TABLE `datastore` (
  `name` varchar(60) NOT NULL,
  `label` varchar(100) NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `datastore`
--

INSERT INTO `datastore` (`name`, `label`, `shared`) VALUES
('property', 'Propriété', 0),
('society_ambulance', 'Ambulance', 1),
('society_police', 'Polizei', 1),
('user_ears', 'Ears', 0),
('user_glasses', 'Glasses', 0),
('user_helmet', 'Helmet', 0),
('user_mask', 'Mask', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `datastore_data`
--

CREATE TABLE `datastore_data` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `owner` varchar(60) DEFAULT NULL,
  `data` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `datastore_data`
--

INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
(1, 'user_ears', 'steam:110000114cf3e89', '{}'),
(2, 'user_glasses', 'steam:110000114cf3e89', '{}'),
(3, 'user_helmet', 'steam:110000114cf3e89', '{}'),
(4, 'user_mask', 'steam:110000114cf3e89', '{}'),
(5, 'user_glasses', 'steam:11000011300960a', '{}'),
(6, 'user_ears', 'steam:11000011300960a', '{}'),
(7, 'user_mask', 'steam:11000011300960a', '{}'),
(8, 'user_helmet', 'steam:11000011300960a', '{}'),
(9, 'society_police', NULL, '{\"weapons\":[{\"count\":1,\"name\":\"WEAPON_ADVANCEDRIFLE\"}]}'),
(10, 'society_ambulance', NULL, '{}'),
(11, 'property', 'steam:110000114cf3e89', '{}'),
(12, 'property', 'steam:11000011300960a', '{}'),
(13, 'property', 'steam:11000010acc835e', '{}'),
(14, 'user_ears', 'steam:11000010acc835e', '{}'),
(15, 'user_mask', 'steam:11000010acc835e', '{}'),
(16, 'user_helmet', 'steam:11000010acc835e', '{}'),
(17, 'user_glasses', 'steam:11000010acc835e', '{}'),
(18, 'property', 'steam:11000013f9a232d', '{}'),
(19, 'user_glasses', 'steam:11000013f9a232d', '{}'),
(20, 'user_ears', 'steam:11000013f9a232d', '{}'),
(21, 'user_helmet', 'steam:11000013f9a232d', '{}'),
(22, 'user_mask', 'steam:11000013f9a232d', '{}'),
(23, 'property', 'steam:11000011b642819', '{\"dressing\":[{\"label\":\"Vagos\",\"skin\":{\"age_1\":0,\"bodyb_1\":0,\"glasses_1\":5,\"decals_1\":0,\"chest_2\":0,\"eyebrows_3\":0,\"moles_2\":0,\"lipstick_4\":0,\"arms\":15,\"lipstick_1\":0,\"hair_1\":21,\"torso_1\":15,\"lipstick_2\":0,\"beard_1\":0,\"sun_1\":0,\"age_2\":0,\"face\":0,\"makeup_4\":0,\"bags_2\":0,\"beard_3\":0,\"sun_2\":0,\"blemishes_1\":0,\"hair_color_2\":0,\"makeup_3\":0,\"bracelets_2\":0,\"bracelets_1\":-1,\"hair_color_1\":0,\"shoes_1\":4,\"bproof_2\":0,\"watches_2\":0,\"lipstick_3\":0,\"complexion_2\":0,\"mask_2\":0,\"pants_1\":42,\"sex\":0,\"blush_2\":0,\"blush_1\":0,\"decals_2\":0,\"beard_2\":0,\"ears_2\":0,\"chain_2\":0,\"watches_1\":-1,\"makeup_2\":0,\"helmet_2\":0,\"tshirt_2\":0,\"chest_1\":0,\"bags_1\":0,\"bodyb_2\":0,\"glasses_2\":0,\"beard_4\":0,\"hair_2\":0,\"pants_2\":5,\"moles_1\":0,\"blemishes_2\":0,\"shoes_2\":0,\"eyebrows_4\":0,\"complexion_1\":0,\"blush_3\":0,\"helmet_1\":56,\"mask_1\":0,\"tshirt_1\":15,\"arms_2\":0,\"skin\":3,\"eyebrows_2\":0,\"chain_1\":50,\"ears_1\":-1,\"torso_2\":0,\"makeup_1\":0,\"eyebrows_1\":0,\"chest_3\":0,\"eye_color\":0,\"bproof_1\":0}},{\"label\":\"Vagos2\",\"skin\":{\"age_1\":0,\"bodyb_1\":0,\"glasses_1\":5,\"decals_1\":0,\"chest_2\":0,\"eyebrows_3\":0,\"moles_2\":0,\"lipstick_4\":0,\"arms\":15,\"lipstick_1\":0,\"hair_1\":21,\"torso_1\":15,\"skin\":3,\"beard_1\":0,\"torso_2\":0,\"age_2\":0,\"face\":0,\"makeup_4\":0,\"ears_2\":0,\"beard_3\":0,\"sun_2\":0,\"blemishes_1\":0,\"bproof_1\":0,\"blush_3\":0,\"bracelets_2\":0,\"bracelets_1\":-1,\"hair_color_1\":0,\"shoes_1\":26,\"bproof_2\":0,\"watches_2\":0,\"lipstick_3\":0,\"moles_1\":0,\"mask_2\":0,\"pants_1\":42,\"sex\":0,\"blush_2\":0,\"blush_1\":0,\"decals_2\":0,\"beard_2\":0,\"chain_1\":50,\"chain_2\":0,\"glasses_2\":0,\"makeup_2\":0,\"tshirt_2\":0,\"chest_1\":0,\"watches_1\":-1,\"bags_1\":0,\"helmet_2\":0,\"lipstick_2\":0,\"beard_4\":0,\"hair_2\":0,\"bags_2\":0,\"pants_2\":5,\"bodyb_2\":0,\"shoes_2\":7,\"complexion_2\":0,\"complexion_1\":0,\"hair_color_2\":0,\"helmet_1\":56,\"mask_1\":0,\"tshirt_1\":15,\"arms_2\":0,\"blemishes_2\":0,\"eyebrows_2\":0,\"ears_1\":-1,\"eyebrows_4\":0,\"sun_1\":0,\"makeup_1\":0,\"eyebrows_1\":0,\"chest_3\":0,\"eye_color\":0,\"makeup_3\":0}}]}'),
(24, 'user_ears', 'steam:11000011b642819', '{}'),
(25, 'user_glasses', 'steam:11000011b642819', '{}'),
(26, 'user_helmet', 'steam:11000011b642819', '{}'),
(27, 'user_mask', 'steam:11000011b642819', '{}'),
(28, 'property', 'steam:1100001102420a8', '{}'),
(29, 'user_ears', 'steam:1100001102420a8', '{}'),
(30, 'user_glasses', 'steam:1100001102420a8', '{}'),
(31, 'user_helmet', 'steam:1100001102420a8', '{}'),
(32, 'user_mask', 'steam:1100001102420a8', '{}'),
(33, 'property', 'steam:11000013f5799ba', '{}'),
(34, 'user_ears', 'steam:11000013f5799ba', '{}'),
(35, 'user_glasses', 'steam:11000013f5799ba', '{}'),
(36, 'user_helmet', 'steam:11000013f5799ba', '{}'),
(37, 'user_mask', 'steam:11000013f5799ba', '{}'),
(38, 'property', 'steam:11000011a0c3cab', '{}'),
(39, 'user_ears', 'steam:11000011a0c3cab', '{}'),
(40, 'user_glasses', 'steam:11000011a0c3cab', '{}'),
(41, 'user_helmet', 'steam:11000011a0c3cab', '{}'),
(42, 'user_mask', 'steam:11000011a0c3cab', '{}'),
(43, 'property', 'steam:110000136dd07fc', '{}'),
(44, 'user_ears', 'steam:110000136dd07fc', '{}'),
(45, 'user_helmet', 'steam:110000136dd07fc', '{}'),
(46, 'user_glasses', 'steam:110000136dd07fc', '{}'),
(47, 'user_mask', 'steam:110000136dd07fc', '{}'),
(48, 'property', 'steam:1100001195c1d46', '{}'),
(49, 'user_ears', 'steam:1100001195c1d46', '{}'),
(50, 'user_glasses', 'steam:1100001195c1d46', '{}'),
(51, 'user_helmet', 'steam:1100001195c1d46', '{}'),
(52, 'user_mask', 'steam:1100001195c1d46', '{}');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `fine_types`
--

CREATE TABLE `fine_types` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `fine_types`
--

INSERT INTO `fine_types` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Missbrauchen der Hupe', 30, 0),
(2, 'Vorfahrtstraße missachtet', 40, 0),
(3, 'Gegenverkehr', 250, 0),
(4, 'Unbefugtes Wenden', 250, 0),
(5, 'Off-Road fahren', 170, 0),
(6, 'Sicherheitsrichtlinien missachtet', 30, 0),
(7, 'Gefährliches halten', 150, 0),
(8, 'Nicht erlaubtes parken', 70, 0),
(9, 'Nicht respektieren des Rechtsfahrgebotes', 70, 0),
(10, 'Nicht einhaltender Priorität eines Rettungsfahrzeuges', 90, 0),
(11, 'Nicht angehalten nach Aufforderung', 105, 0),
(12, 'Bei Rot über die Ampel', 130, 0),
(13, 'gefährliches Überholmanöver', 100, 0),
(14, 'Fahrzeug nicht Verkehrstauglich', 100, 0),
(15, 'Fahren ohne Lizenz', 1500, 0),
(16, 'Hit and Run', 800, 0),
(17, 'zu schnell < 5 kmh', 90, 0),
(18, 'zu schnell 5-15 kmh', 120, 0),
(19, 'zu schnell 15-30 kmh', 180, 0),
(20, 'zu schnell > 30 kmh', 300, 0),
(21, 'Verkehrsbehinderung', 110, 1),
(22, 'Nicht Benutzung öffentlicher Straßen', 90, 1),
(23, 'Störung der öffentlichen Ordnung', 90, 1),
(24, 'Polizeieinsatz behindern', 130, 1),
(25, 'Beildigung an Zivilist', 75, 1),
(26, 'Polizist geringschätzen', 110, 1),
(27, 'verbale Bedrohung an Zivilist', 90, 1),
(28, 'verbale Bedrohung an Polizist', 150, 1),
(29, 'Illegale Demonstration', 250, 1),
(30, 'versuchte Bestechung', 1500, 1),
(31, 'Mit Messer in der Stadt bewaffnet', 120, 2),
(32, 'Mit tötlicher Waffe bewaffnet', 300, 2),
(33, 'Unbefugter Besitz einer Waffe (Standard-Lizenz)', 600, 2),
(34, 'Illegale Waffe', 700, 2),
(35, 'Einbruch/Aufbruch', 300, 2),
(36, 'Autodiebstahl', 1800, 2),
(37, 'Drogenverkauf', 1500, 2),
(38, 'Drogen herstellen', 1500, 2),
(39, 'Drogenbesitz', 650, 2),
(40, 'Zivile Geisel genommen', 1500, 2),
(41, 'Geiselnahme', 2000, 2),
(42, 'Waghalsiges Lenken', 650, 2),
(43, 'Ladenraub', 650, 2),
(44, 'Bankraub', 1500, 2),
(45, 'Schießen auf Zivilisten', 2000, 3),
(46, 'Schießen auf Beamten', 2500, 3),
(47, 'versuchterMord an Zivilisten', 3000, 3),
(48, 'versuchter Mord an einem Beamten', 5000, 3),
(49, 'Mord an Zivilisten', 10000, 3),
(50, 'Mord an Beamten', 30000, 3),
(51, 'versehentlicher Mord', 1800, 3),
(52, 'Firmenbetrug', 2000, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `import_vehicles`
--

CREATE TABLE `import_vehicles` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `import_vehicles`
--

INSERT INTO `import_vehicles` (`name`, `model`, `price`, `category`) VALUES
('Blista', 'blista', 69000, 'coupe'),
('Buffalo', 'buffalo', 100000, 'sport'),
('Jackal', 'jackal', 95000, 'coupe'),
('Zion', 'zion', 98000, 'coupe');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `import_vehicle_categories`
--

CREATE TABLE `import_vehicle_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `import_vehicle_categories`
--

INSERT INTO `import_vehicle_categories` (`name`, `label`) VALUES
('compacts', 'Compacts'),
('coupe', 'Coupés'),
('motorcycles', 'Motos'),
('muscle', 'Muscle'),
('offroad', 'Off Road'),
('sedans', 'Sedans'),
('sport', 'Sport'),
('sportsclassics', 'Sports Classics'),
('super', 'Super'),
('suvs', 'SUVs'),
('vans', 'Vans');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `items`
--

CREATE TABLE `items` (
  `name` varchar(50) NOT NULL,
  `label` varchar(50) NOT NULL,
  `weight` int(11) NOT NULL DEFAULT 1,
  `rare` tinyint(1) NOT NULL DEFAULT 0,
  `can_remove` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `items`
--

INSERT INTO `items` (`name`, `label`, `weight`, `rare`, `can_remove`) VALUES
('alive_chicken', 'lebendes Huhn', 20, 0, 1),
('bandage', 'Bandage', 2, 0, 1),
('bottle', 'Pfandflasche', 1, 0, 1),
('bread', 'Brot', 1, 0, 1),
('clothe', 'Kleidung', 40, 0, 1),
('copper', 'Kupfer', 56, 0, 1),
('cutted_wood', 'Holzstämme', 20, 0, 1),
('diamond', 'Diamant', 50, 0, 1),
('essence', 'Benzin', 24, 0, 1),
('fabric', 'Tuch', 80, 0, 1),
('fish', 'Fisch', 100, 0, 1),
('gold', 'Gold', 21, 0, 1),
('iron', 'Eisen', 42, 0, 1),
('medikit', 'Medikit', 2, 0, 1),
('packaged_chicken', 'Hähnchenfilet', 100, 0, 1),
('packaged_plank', 'Bretterpaket', 100, 0, 1),
('petrol', 'Öl', 24, 0, 1),
('petrol_raffin', 'bearbeitetes Öl', 24, 0, 1),
('phone', 'Handy', 1, 0, 1),
('slaughtered_chicken', 'geschlachtetes Huhn', 20, 0, 1),
('stone', 'Felsbrocken', 7, 0, 1),
('washed_stone', 'gewaschener Felsbrocken', 7, 0, 1),
('water', 'Wasser', 1, 0, 1),
('whool', 'Wolle', 40, 0, 1),
('wood', 'Holz', 20, 0, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `jobs`
--

CREATE TABLE `jobs` (
  `name` varchar(50) NOT NULL,
  `label` varchar(50) DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `jobs`
--

INSERT INTO `jobs` (`name`, `label`, `whitelisted`) VALUES
('ambulance', 'Ambulance', 1),
('cardealer', 'Händler', 1),
('fisherman', 'Fischerei', 0),
('fueler', 'Raffinerie', 0),
('lumberjack', 'Holzbetrieb', 0),
('miner', 'Bergbau', 0),
('police', 'Polizei', 1),
('reporter', 'Kanal 7', 0),
('slaughterer', 'Schlachterei', 0),
('tailor', 'Schneiderei', 0),
('unemployed', 'Arbeitlos', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `job_grades`
--

CREATE TABLE `job_grades` (
  `id` int(11) NOT NULL,
  `job_name` varchar(50) DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `label` varchar(50) NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext NOT NULL,
  `skin_female` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `job_grades`
--

INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
(1, 'unemployed', 0, 'unemployed', 'Hartz4', 200, '{}', '{}'),
(2, 'cardealer', 0, 'recruit', 'Neuling', 4500, '{}', '{}'),
(3, 'cardealer', 1, 'novice', 'Verkäufer', 6400, '{}', '{}'),
(4, 'cardealer', 2, 'experienced', 'Verkaufsleiter', 6800, '{}', '{}'),
(5, 'cardealer', 3, 'boss', 'Boss', 7500, '{}', '{}'),
(6, 'police', 0, 'recruit', 'Rekrut', 2500, '{}', '{}'),
(7, 'police', 1, 'officer', 'Officier', 3500, '{}', '{}'),
(8, 'police', 2, 'sergeant', 'Sergent', 5500, '{}', '{}'),
(9, 'police', 3, 'lieutenant', 'Lieutenant', 6500, '{}', '{}'),
(10, 'police', 4, 'boss', 'Commandant', 7500, '{}', '{}'),
(11, 'ambulance', 0, 'ambulance', 'Ambulancier', 20, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(12, 'ambulance', 1, 'doctor', 'Medecin', 40, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(13, 'ambulance', 2, 'chief_doctor', 'Medecin-chef', 60, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(14, 'ambulance', 3, 'boss', 'Chirurgien', 80, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(15, 'lumberjack', 0, 'employee', 'Intérimaire', 0, '{}', '{}'),
(16, 'fisherman', 0, 'employee', 'Intérimaire', 0, '{}', '{}'),
(17, 'fueler', 0, 'employee', 'Intérimaire', 0, '{}', '{}'),
(18, 'reporter', 0, 'employee', 'Intérimaire', 0, '{}', '{}'),
(19, 'tailor', 0, 'employee', 'Intérimaire', 0, '{\"mask_1\":0,\"arms\":1,\"glasses_1\":0,\"hair_color_2\":4,\"makeup_1\":0,\"face\":19,\"glasses\":0,\"mask_2\":0,\"makeup_3\":0,\"skin\":29,\"helmet_2\":0,\"lipstick_4\":0,\"sex\":0,\"torso_1\":24,\"makeup_2\":0,\"bags_2\":0,\"chain_2\":0,\"ears_1\":-1,\"bags_1\":0,\"bproof_1\":0,\"shoes_2\":0,\"lipstick_2\":0,\"chain_1\":0,\"tshirt_1\":0,\"eyebrows_3\":0,\"pants_2\":0,\"beard_4\":0,\"torso_2\":0,\"beard_2\":6,\"ears_2\":0,\"hair_2\":0,\"shoes_1\":36,\"tshirt_2\":0,\"beard_3\":0,\"hair_1\":2,\"hair_color_1\":0,\"pants_1\":48,\"helmet_1\":-1,\"bproof_2\":0,\"eyebrows_4\":0,\"eyebrows_2\":0,\"decals_1\":0,\"age_2\":0,\"beard_1\":5,\"shoes\":10,\"lipstick_1\":0,\"eyebrows_1\":0,\"glasses_2\":0,\"makeup_4\":0,\"decals_2\":0,\"lipstick_3\":0,\"age_1\":0}', '{\"mask_1\":0,\"arms\":5,\"glasses_1\":5,\"hair_color_2\":4,\"makeup_1\":0,\"face\":19,\"glasses\":0,\"mask_2\":0,\"makeup_3\":0,\"skin\":29,\"helmet_2\":0,\"lipstick_4\":0,\"sex\":1,\"torso_1\":52,\"makeup_2\":0,\"bags_2\":0,\"chain_2\":0,\"ears_1\":-1,\"bags_1\":0,\"bproof_1\":0,\"shoes_2\":1,\"lipstick_2\":0,\"chain_1\":0,\"tshirt_1\":23,\"eyebrows_3\":0,\"pants_2\":0,\"beard_4\":0,\"torso_2\":0,\"beard_2\":6,\"ears_2\":0,\"hair_2\":0,\"shoes_1\":42,\"tshirt_2\":4,\"beard_3\":0,\"hair_1\":2,\"hair_color_1\":0,\"pants_1\":36,\"helmet_1\":-1,\"bproof_2\":0,\"eyebrows_4\":0,\"eyebrows_2\":0,\"decals_1\":0,\"age_2\":0,\"beard_1\":5,\"shoes\":10,\"lipstick_1\":0,\"eyebrows_1\":0,\"glasses_2\":0,\"makeup_4\":0,\"decals_2\":0,\"lipstick_3\":0,\"age_1\":0}'),
(20, 'miner', 0, 'employee', 'Intérimaire', 0, '{\"tshirt_2\":1,\"ears_1\":8,\"glasses_1\":15,\"torso_2\":0,\"ears_2\":2,\"glasses_2\":3,\"shoes_2\":1,\"pants_1\":75,\"shoes_1\":51,\"bags_1\":0,\"helmet_2\":0,\"pants_2\":7,\"torso_1\":71,\"tshirt_1\":59,\"arms\":2,\"bags_2\":0,\"helmet_1\":0}', '{}'),
(21, 'slaughterer', 0, 'employee', 'Intérimaire', 0, '{\"age_1\":0,\"glasses_2\":0,\"beard_1\":5,\"decals_2\":0,\"beard_4\":0,\"shoes_2\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"hair_2\":0,\"arms\":67,\"pants_1\":36,\"skin\":29,\"eyebrows_2\":0,\"shoes\":10,\"helmet_1\":-1,\"lipstick_1\":0,\"helmet_2\":0,\"hair_color_1\":0,\"glasses\":0,\"makeup_4\":0,\"makeup_1\":0,\"hair_1\":2,\"bproof_1\":0,\"bags_1\":0,\"mask_1\":0,\"lipstick_3\":0,\"chain_1\":0,\"eyebrows_4\":0,\"sex\":0,\"torso_1\":56,\"beard_2\":6,\"shoes_1\":12,\"decals_1\":0,\"face\":19,\"lipstick_4\":0,\"tshirt_1\":15,\"mask_2\":0,\"age_2\":0,\"eyebrows_3\":0,\"chain_2\":0,\"glasses_1\":0,\"ears_1\":-1,\"bags_2\":0,\"ears_2\":0,\"torso_2\":0,\"bproof_2\":0,\"makeup_2\":0,\"eyebrows_1\":0,\"makeup_3\":0,\"pants_2\":0,\"beard_3\":0,\"hair_color_2\":4}', '{\"age_1\":0,\"glasses_2\":0,\"beard_1\":5,\"decals_2\":0,\"beard_4\":0,\"shoes_2\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"hair_2\":0,\"arms\":72,\"pants_1\":45,\"skin\":29,\"eyebrows_2\":0,\"shoes\":10,\"helmet_1\":-1,\"lipstick_1\":0,\"helmet_2\":0,\"hair_color_1\":0,\"glasses\":0,\"makeup_4\":0,\"makeup_1\":0,\"hair_1\":2,\"bproof_1\":0,\"bags_1\":0,\"mask_1\":0,\"lipstick_3\":0,\"chain_1\":0,\"eyebrows_4\":0,\"sex\":1,\"torso_1\":49,\"beard_2\":6,\"shoes_1\":24,\"decals_1\":0,\"face\":19,\"lipstick_4\":0,\"tshirt_1\":9,\"mask_2\":0,\"age_2\":0,\"eyebrows_3\":0,\"chain_2\":0,\"glasses_1\":5,\"ears_1\":-1,\"bags_2\":0,\"ears_2\":0,\"torso_2\":0,\"bproof_2\":0,\"makeup_2\":0,\"eyebrows_1\":0,\"makeup_3\":0,\"pants_2\":0,\"beard_3\":0,\"hair_color_2\":4}');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `licenses`
--

CREATE TABLE `licenses` (
  `type` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `licenses`
--

INSERT INTO `licenses` (`type`, `label`) VALUES
('dmv', 'Führerschein'),
('drive', 'PKW Führerschein'),
('drive_bike', 'Motorrad Führerschein'),
('drive_truck', 'LKW Führerschein');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `owned_properties`
--

CREATE TABLE `owned_properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` double NOT NULL,
  `rented` int(11) NOT NULL,
  `owner` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `owned_vehicles`
--

CREATE TABLE `owned_vehicles` (
  `owner` varchar(30) NOT NULL,
  `plate` varchar(12) NOT NULL,
  `vehicle` longtext NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'car',
  `job` varchar(20) NOT NULL DEFAULT '',
  `stored` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `owned_vehicles`
--

INSERT INTO `owned_vehicles` (`owner`, `plate`, `vehicle`, `type`, `job`, `stored`) VALUES
('steam:110000114cf3e89', 'GCSE77', '{\"extras\":{\"1\":true},\"modAerials\":-1,\"modLivery\":0,\"modSideSkirt\":-1,\"modBrakes\":-1,\"modEngine\":-1,\"fuelLevel\":27.1,\"bodyHealth\":1000.0,\"neonColor\":[255,0,255],\"modWindows\":-1,\"modTrimA\":-1,\"modSeats\":-1,\"wheelColor\":0,\"modExhaust\":-1,\"modBackWheels\":-1,\"modStruts\":-1,\"modRoof\":-1,\"color1\":134,\"modTrimB\":-1,\"modFrame\":-1,\"modArmor\":-1,\"modAirFilter\":-1,\"modSmokeEnabled\":false,\"modSpoilers\":-1,\"modOrnaments\":-1,\"modSteeringWheel\":-1,\"modFrontWheels\":-1,\"modAPlate\":-1,\"pearlescentColor\":0,\"modArchCover\":-1,\"modHydrolic\":-1,\"modFender\":-1,\"plateIndex\":4,\"modTurbo\":false,\"modEngineBlock\":-1,\"plate\":\"GCSE77\",\"modDoorSpeaker\":-1,\"modTank\":-1,\"modDashboard\":-1,\"tyreSmokeColor\":[255,255,255],\"modRightFender\":-1,\"modSuspension\":-1,\"modRearBumper\":-1,\"color2\":134,\"modTrunk\":-1,\"modShifterLeavers\":-1,\"model\":699188170,\"engineHealth\":1000.0,\"modVanityPlate\":-1,\"modTransmission\":-1,\"modXenon\":false,\"modDial\":-1,\"modHood\":-1,\"wheels\":0,\"modPlateHolder\":-1,\"windowTint\":-1,\"dirtLevel\":1.0,\"modSpeakers\":-1,\"modFrontBumper\":-1,\"modHorns\":-1,\"modGrille\":-1,\"neonEnabled\":[false,false,false,false]}', 'car', 'police', 1),
('steam:110000114cf3e89', 'IIX 166', '{\"modSmokeEnabled\":false,\"model\":-1205801634,\"modSpoilers\":-1,\"modSeats\":-1,\"modEngineBlock\":-1,\"modHorns\":-1,\"tyreSmokeColor\":[255,255,255],\"dirtLevel\":5.0,\"modTrunk\":-1,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"modXenon\":false,\"modSteeringWheel\":-1,\"engineHealth\":1000.0,\"modBrakes\":-1,\"modFender\":-1,\"modExhaust\":-1,\"modTransmission\":-1,\"modPlateHolder\":-1,\"modDashboard\":-1,\"modArchCover\":-1,\"modAerials\":-1,\"modWindows\":-1,\"modHydrolic\":-1,\"modRightFender\":-1,\"modAPlate\":-1,\"modRearBumper\":-1,\"modEngine\":-1,\"modStruts\":-1,\"modDial\":-1,\"modBackWheels\":-1,\"modSpeakers\":-1,\"extras\":[],\"plate\":\"IIX 166\",\"modVanityPlate\":-1,\"modTurbo\":false,\"modGrille\":-1,\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"color1\":1,\"modLivery\":-1,\"modArmor\":-1,\"modHood\":-1,\"fuelLevel\":65.0,\"windowTint\":-1,\"modRoof\":-1,\"modTrimB\":-1,\"wheels\":1,\"neonColor\":[255,0,255],\"color2\":0,\"modFrontBumper\":-1,\"pearlescentColor\":6,\"modFrontWheels\":-1,\"modOrnaments\":-1,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modSideSkirt\":-1,\"wheelColor\":156,\"plateIndex\":2,\"modAirFilter\":-1,\"modTank\":-1,\"bodyHealth\":1000.0}', 'car', '', 1),
('steam:110000114cf3e89', 'JPWD50', '{\"modBrakes\":-1,\"modArmor\":-1,\"modTransmission\":-1,\"windowTint\":-1,\"pearlescentColor\":1,\"modVanityPlate\":-1,\"modSmokeEnabled\":false,\"modPlateHolder\":-1,\"modTank\":-1,\"modOrnaments\":-1,\"wheels\":0,\"modDashboard\":-1,\"modSpeakers\":-1,\"plateIndex\":4,\"modFrontWheels\":-1,\"modTurbo\":false,\"modArchCover\":-1,\"fuelLevel\":65.0,\"modRearBumper\":-1,\"modStruts\":-1,\"wheelColor\":156,\"modEngine\":-1,\"extras\":{\"4\":true,\"1\":true},\"modSeats\":-1,\"model\":1912215274,\"modBackWheels\":-1,\"modRoof\":-1,\"modSideSkirt\":-1,\"modAirFilter\":-1,\"modLivery\":1,\"modSpoilers\":-1,\"modAerials\":-1,\"plate\":\"JPWD50\",\"modHorns\":-1,\"modSteeringWheel\":-1,\"color1\":111,\"modTrimA\":-1,\"modHood\":-1,\"modXenon\":false,\"modHydrolic\":-1,\"color2\":0,\"neonEnabled\":[false,false,false,false],\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"modDial\":-1,\"modFender\":-1,\"neonColor\":[255,0,255],\"modWindows\":-1,\"modGrille\":-1,\"bodyHealth\":1000.0,\"dirtLevel\":2.0,\"modFrame\":-1,\"modTrunk\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrimB\":-1,\"modSuspension\":-1,\"modRightFender\":-1,\"modFrontBumper\":-1,\"modExhaust\":-1,\"modAPlate\":-1,\"engineHealth\":1000.0,\"modDoorSpeaker\":-1}', 'car', 'police', 1),
('steam:110000114cf3e89', 'KNVP55', '{\"color1\":134,\"plateIndex\":2,\"modSuspension\":-1,\"modTank\":-1,\"modFender\":-1,\"modSideSkirt\":-1,\"modAPlate\":-1,\"modGrille\":-1,\"windowTint\":-1,\"plate\":\"KNVP55\",\"modTransmission\":-1,\"modTrimA\":-1,\"extras\":{\"1\":false},\"modWindows\":-1,\"modDashboard\":-1,\"color2\":3,\"modShifterLeavers\":-1,\"modArchCover\":-1,\"modHood\":-1,\"modFrame\":-1,\"modTrimB\":-1,\"pearlescentColor\":0,\"modVanityPlate\":-1,\"wheelColor\":0,\"dirtLevel\":3.0,\"modDoorSpeaker\":-1,\"modArmor\":-1,\"modHorns\":-1,\"bodyHealth\":1000.0,\"modEngine\":-1,\"modStruts\":-1,\"modLivery\":1,\"fuelLevel\":65.0,\"modFrontWheels\":-1,\"modSteeringWheel\":-1,\"modOrnaments\":-1,\"tyreSmokeColor\":[255,255,255],\"modSpoilers\":-1,\"neonEnabled\":[false,false,false,false],\"modAirFilter\":-1,\"modRoof\":-1,\"modSmokeEnabled\":false,\"modAerials\":-1,\"modRearBumper\":-1,\"modHydrolic\":-1,\"modSeats\":-1,\"modPlateHolder\":-1,\"modDial\":-1,\"modRightFender\":-1,\"modXenon\":false,\"modBackWheels\":-1,\"wheels\":0,\"modTrunk\":-1,\"neonColor\":[255,0,255],\"modFrontBumper\":-1,\"engineHealth\":1000.0,\"modSpeakers\":-1,\"modTurbo\":false,\"modExhaust\":-1,\"modEngineBlock\":-1,\"model\":992657080,\"modBrakes\":-1}', 'car', '', 1),
('steam:110000114cf3e89', 'NQOZ65', '{\"plate\":\"NQOZ65\",\"modDoorSpeaker\":-1,\"windowTint\":-1,\"wheels\":0,\"modExhaust\":-1,\"modHorns\":-1,\"modLivery\":0,\"modAirFilter\":-1,\"plateIndex\":4,\"dirtLevel\":9.0,\"modHydrolic\":-1,\"modSuspension\":-1,\"neonColor\":[255,0,255],\"modRearBumper\":-1,\"modWindows\":-1,\"modSpeakers\":-1,\"modDial\":-1,\"modSideSkirt\":-1,\"model\":699188170,\"modDashboard\":-1,\"modGrille\":-1,\"modTank\":-1,\"modTrimB\":-1,\"modSteeringWheel\":-1,\"fuelLevel\":50.5,\"modFrontBumper\":-1,\"modEngineBlock\":-1,\"color1\":134,\"modAerials\":-1,\"modFrame\":-1,\"modFender\":-1,\"extras\":{\"1\":true},\"modArchCover\":-1,\"modRightFender\":-1,\"modOrnaments\":-1,\"engineHealth\":1000.0,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"modPlateHolder\":-1,\"modStruts\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modTurbo\":false,\"modTrunk\":-1,\"bodyHealth\":1000.0,\"modBackWheels\":-1,\"modTransmission\":-1,\"modRoof\":-1,\"modSeats\":-1,\"modBrakes\":-1,\"modSpoilers\":-1,\"modShifterLeavers\":-1,\"modSmokeEnabled\":false,\"modArmor\":-1,\"pearlescentColor\":0,\"modXenon\":false,\"modAPlate\":-1,\"modHood\":-1,\"modFrontWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"color2\":134,\"wheelColor\":0}', 'car', 'police', 1),
('steam:110000114cf3e89', 'NTRH16', '{\"modTrimA\":-1,\"modFrontBumper\":-1,\"modWindows\":-1,\"modHorns\":-1,\"tyreSmokeColor\":[255,255,255],\"modTurbo\":false,\"modDial\":-1,\"modSeats\":-1,\"modShifterLeavers\":-1,\"modAirFilter\":-1,\"modRoof\":-1,\"extras\":{\"1\":true},\"modPlateHolder\":-1,\"modLivery\":0,\"modExhaust\":-1,\"modBrakes\":-1,\"modSteeringWheel\":-1,\"plate\":\"NTRH16\",\"engineHealth\":1000.0,\"modSmokeEnabled\":false,\"modSpoilers\":-1,\"modAerials\":-1,\"modVanityPlate\":-1,\"modSpeakers\":-1,\"modSuspension\":-1,\"bodyHealth\":1000.0,\"modBackWheels\":-1,\"fuelLevel\":23.9,\"pearlescentColor\":0,\"plateIndex\":4,\"modAPlate\":-1,\"modRearBumper\":-1,\"neonColor\":[255,0,255],\"modTank\":-1,\"modArmor\":-1,\"modFrame\":-1,\"modEngineBlock\":-1,\"color2\":134,\"color1\":134,\"model\":699188170,\"modHydrolic\":-1,\"modRightFender\":-1,\"neonEnabled\":[false,false,false,false],\"modSideSkirt\":-1,\"modTransmission\":-1,\"modStruts\":-1,\"modXenon\":false,\"modGrille\":-1,\"dirtLevel\":2.0,\"modTrimB\":-1,\"modDashboard\":-1,\"modHood\":-1,\"windowTint\":-1,\"wheelColor\":0,\"modDoorSpeaker\":-1,\"modArchCover\":-1,\"modFrontWheels\":-1,\"modOrnaments\":-1,\"wheels\":0,\"modTrunk\":-1,\"modEngine\":-1,\"modFender\":-1}', 'car', 'police', 1),
('steam:110000114cf3e89', 'UZB 099', '{\"plateIndex\":3,\"modTank\":-1,\"color1\":1,\"modVanityPlate\":-1,\"modArmor\":-1,\"wheelColor\":156,\"modRoof\":-1,\"modWindows\":-1,\"modTransmission\":-1,\"modDashboard\":-1,\"modHood\":-1,\"plate\":\"UZB 099\",\"modFrontWheels\":-1,\"modHydrolic\":-1,\"modSpoilers\":-1,\"pearlescentColor\":6,\"modTurbo\":false,\"fuelLevel\":65.0,\"modRightFender\":-1,\"modGrille\":-1,\"modAPlate\":-1,\"modFrame\":-1,\"modXenon\":false,\"modFender\":-1,\"modSuspension\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrontBumper\":-1,\"engineHealth\":1000.0,\"modEngineBlock\":-1,\"modExhaust\":-1,\"modAirFilter\":-1,\"windowTint\":-1,\"modPlateHolder\":-1,\"modSteeringWheel\":-1,\"modDial\":-1,\"bodyHealth\":1000.0,\"modBrakes\":-1,\"dirtLevel\":10.0,\"modRearBumper\":-1,\"modSeats\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTrimB\":-1,\"modLivery\":-1,\"modHorns\":-1,\"modDoorSpeaker\":-1,\"modArchCover\":-1,\"modOrnaments\":-1,\"modAerials\":-1,\"modEngine\":-1,\"modBackWheels\":-1,\"color2\":0,\"modStruts\":-1,\"modSmokeEnabled\":false,\"extras\":[],\"model\":-1205801634,\"neonEnabled\":[false,false,false,false],\"neonColor\":[255,0,255],\"wheels\":1,\"modSideSkirt\":-1,\"modTrimA\":-1,\"modTrunk\":-1}', 'car', '', 1),
('steam:110000114cf3e89', 'VDH 501', '{\"modTrimA\":-1,\"modRightFender\":-1,\"modHood\":-1,\"modDoorSpeaker\":-1,\"modTrunk\":-1,\"modBrakes\":-1,\"modSmokeEnabled\":false,\"wheels\":1,\"modVanityPlate\":-1,\"modFrontWheels\":-1,\"modDashboard\":-1,\"modStruts\":-1,\"modEngine\":-1,\"modRoof\":-1,\"modGrille\":-1,\"extras\":[],\"neonEnabled\":[false,false,false,false],\"modArmor\":-1,\"modAPlate\":-1,\"modEngineBlock\":-1,\"modLivery\":-1,\"color1\":1,\"modShifterLeavers\":-1,\"plateIndex\":3,\"dirtLevel\":6.0,\"modFrontBumper\":-1,\"pearlescentColor\":6,\"modTrimB\":-1,\"modSideSkirt\":-1,\"modBackWheels\":-1,\"modTransmission\":-1,\"modSpeakers\":-1,\"modFrame\":-1,\"modSuspension\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"engineHealth\":1000.0,\"modPlateHolder\":-1,\"modHorns\":-1,\"modRearBumper\":-1,\"model\":-1205801634,\"bodyHealth\":1000.0,\"modArchCover\":-1,\"tyreSmokeColor\":[255,255,255],\"plate\":\"VDH 501\",\"modDial\":-1,\"modHydrolic\":-1,\"modTank\":-1,\"color2\":0,\"modFender\":-1,\"modAirFilter\":-1,\"modXenon\":false,\"wheelColor\":156,\"neonColor\":[255,0,255],\"modSteeringWheel\":-1,\"modWindows\":-1,\"modTurbo\":false,\"modAerials\":-1,\"fuelLevel\":65.0,\"modSpoilers\":-1,\"windowTint\":-1,\"modExhaust\":-1}', 'car', '', 1),
('steam:11000011a0c3cab', 'WOBS00', '{\"modEngine\":-1,\"modAirFilter\":-1,\"modHood\":-1,\"modRearBumper\":-1,\"modSpoilers\":-1,\"modSpeakers\":-1,\"modHydrolic\":-1,\"modTrimA\":-1,\"modStruts\":-1,\"wheelColor\":156,\"modFender\":-1,\"modDashboard\":-1,\"modSideSkirt\":-1,\"modArchCover\":-1,\"modXenon\":false,\"modSmokeEnabled\":false,\"modRightFender\":-1,\"modShifterLeavers\":-1,\"modFrontWheels\":-1,\"plate\":\"WOBS00\",\"modOrnaments\":-1,\"modPlateHolder\":-1,\"modExhaust\":-1,\"modFrame\":-1,\"model\":-586459613,\"modSteeringWheel\":-1,\"tyreSmokeColor\":[255,255,255],\"bodyHealth\":998.3,\"fuelLevel\":65.0,\"modEngineBlock\":-1,\"engineHealth\":1000.0,\"neonColor\":[255,0,255],\"modArmor\":-1,\"modTank\":-1,\"neonEnabled\":[false,false,false,false],\"modAPlate\":-1,\"modHorns\":-1,\"modLivery\":-1,\"modTrunk\":-1,\"extras\":{\"12\":false,\"10\":true},\"modFrontBumper\":-1,\"modDoorSpeaker\":-1,\"modTurbo\":false,\"modVanityPlate\":-1,\"modTransmission\":-1,\"modDial\":-1,\"modBackWheels\":-1,\"modTrimB\":-1,\"modAerials\":-1,\"dirtLevel\":7.4,\"modBrakes\":-1,\"color2\":0,\"wheels\":7,\"modSeats\":-1,\"plateIndex\":1,\"modSuspension\":-1,\"color1\":0,\"pearlescentColor\":0,\"modRoof\":-1,\"modWindows\":-1,\"windowTint\":-1,\"modGrille\":-1}', 'car', '', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `phone_app_chat`
--

CREATE TABLE `phone_app_chat` (
  `id` int(11) NOT NULL,
  `channel` varchar(20) NOT NULL,
  `message` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `phone_calls`
--

CREATE TABLE `phone_calls` (
  `id` int(11) NOT NULL,
  `owner` varchar(10) NOT NULL COMMENT 'Num tel proprio',
  `num` varchar(10) NOT NULL COMMENT 'Num reférence du contact',
  `incoming` int(11) NOT NULL COMMENT 'Défini si on est à l''origine de l''appels',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `accepts` int(11) NOT NULL COMMENT 'Appels accepter ou pas'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `phone_calls`
--

INSERT INTO `phone_calls` (`id`, `owner`, `num`, `incoming`, `time`, `accepts`) VALUES
(122, '827-5849', '906-8228', 1, '2020-02-15 21:55:14', 1),
(123, '906-8228', '827-5849', 0, '2020-02-15 21:55:14', 1),
(124, '827-5849', '906-8228', 1, '2020-02-15 21:55:17', 1),
(125, '906-8228', '827-5849', 0, '2020-02-15 21:55:17', 1),
(126, '827-5849', '906-8228', 1, '2020-02-15 21:55:17', 1),
(127, '906-8228', '827-5849', 0, '2020-02-15 21:55:17', 1),
(128, '827-5849', '906-8228', 1, '2020-02-15 21:56:36', 1),
(129, '906-8228', '827-5849', 0, '2020-02-15 21:56:36', 1),
(130, '827-5849', '906-8228', 1, '2020-02-15 21:56:37', 1),
(131, '906-8228', '827-5849', 0, '2020-02-15 21:56:37', 1),
(132, '827-5849', '906-8228', 1, '2020-02-15 22:01:00', 1),
(133, '906-8228', '827-5849', 0, '2020-02-15 22:01:00', 1),
(134, '827-5849', '906-8228', 1, '2020-02-15 22:02:19', 1),
(135, '906-8228', '827-5849', 0, '2020-02-15 22:02:19', 1),
(136, '827-5849', '906-8228', 1, '2020-02-15 22:07:55', 0),
(137, '906-8228', '827-5849', 0, '2020-02-15 22:07:55', 0),
(138, '827-5849', '906-8228', 1, '2020-02-15 22:10:02', 0),
(139, '906-8228', '827-5849', 0, '2020-02-15 22:10:02', 0),
(140, '827-5849', '906-8228', 1, '2020-02-15 22:11:05', 1),
(141, '906-8228', '827-5849', 0, '2020-02-15 22:11:05', 1),
(142, '827-5849', '906-8228', 1, '2020-02-15 22:18:04', 1),
(143, '906-8228', '827-5849', 0, '2020-02-15 22:18:04', 1),
(144, '906-8228', '827-5849', 1, '2020-02-15 22:18:17', 1),
(145, '827-5849', '906-8228', 0, '2020-02-15 22:18:17', 1),
(146, '906-8228', '827-5849', 0, '2020-02-15 22:23:58', 1),
(147, '827-5849', '906-8228', 1, '2020-02-15 22:23:58', 1),
(148, '906-8228', '827-5849', 0, '2020-02-15 22:30:26', 1),
(149, '827-5849', '906-8228', 1, '2020-02-15 22:30:26', 1),
(150, '827-5849', '906-8228', 1, '2020-02-15 22:30:34', 1),
(151, '906-8228', '827-5849', 0, '2020-02-15 22:30:34', 1),
(152, '906-8228', '827-5849', 1, '2020-02-15 22:32:54', 0),
(153, '827-5849', '906-8228', 0, '2020-02-15 22:32:54', 0),
(154, '906-8228', '827-5849', 1, '2020-02-15 22:33:17', 1),
(155, '827-5849', '906-8228', 0, '2020-02-15 22:33:17', 1),
(156, '827-5849', '906-8228', 1, '2020-02-15 22:33:21', 1),
(157, '906-8228', '827-5849', 0, '2020-02-15 22:33:21', 1),
(158, '906-8228', '827-5849', 0, '2020-02-16 09:14:16', 0),
(159, '827-5849', '906-8228', 1, '2020-02-16 09:14:16', 0),
(160, '827-5849', '906-8228', 1, '2020-02-16 13:40:00', 0),
(161, '906-8228', '827-5849', 0, '2020-02-16 13:40:00', 0),
(162, '827-5849', '906-8228', 1, '2020-02-16 13:41:07', 0),
(163, '906-8228', '827-5849', 0, '2020-02-16 13:41:07', 0),
(164, '827-5849', '906-8228', 1, '2020-02-16 13:45:03', 1),
(165, '906-8228', '827-5849', 0, '2020-02-16 13:45:03', 1),
(166, '414-5162', '911', 1, '2020-02-16 19:42:28', 0),
(167, '463-7433', '414-5162', 1, '2020-02-16 19:49:35', 1),
(168, '414-5162', '463-7433', 0, '2020-02-16 19:49:35', 1),
(169, '463-7433', '911', 1, '2020-02-16 19:50:06', 0),
(170, '835-5913', '911', 1, '2020-02-17 15:45:44', 0),
(171, '835-5913', '911', 1, '2020-02-17 15:45:55', 0),
(172, '463-7433', '414-5162', 1, '2020-02-17 20:02:24', 0),
(173, '414-5162', '463-7433', 0, '2020-02-17 20:02:24', 0),
(174, '463-7433', '414-5162', 1, '2020-02-17 20:02:35', 0),
(175, '414-5162', '463-7433', 0, '2020-02-17 20:02:35', 0),
(176, '463-7433', '835-5913', 1, '2020-02-17 20:35:15', 1),
(177, '835-5913', '463-7433', 0, '2020-02-17 20:35:15', 1),
(178, '835-5913', '463-7433', 0, '2020-02-17 20:35:16', 1),
(179, '463-7433', '835-5913', 1, '2020-02-17 20:35:16', 1),
(180, '463-7433', '911', 1, '2020-02-17 20:36:00', 0),
(181, '835-5913', '911', 1, '2020-02-17 20:36:18', 0),
(182, '835-5913', '911', 1, '2020-02-17 20:36:48', 1),
(183, '463-7433', '911', 0, '2020-02-17 20:37:29', 1),
(184, '463-7433', '911', 0, '2020-02-17 20:37:30', 1),
(185, '463-7433', 'police', 1, '2020-02-17 20:53:25', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `phone_messages`
--

CREATE TABLE `phone_messages` (
  `id` int(11) NOT NULL,
  `transmitter` varchar(10) NOT NULL,
  `receiver` varchar(10) NOT NULL,
  `message` varchar(255) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `isRead` int(11) NOT NULL DEFAULT 0,
  `owner` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `phone_messages`
--

INSERT INTO `phone_messages` (`id`, `transmitter`, `receiver`, `message`, `time`, `isRead`, `owner`) VALUES
(106, '911', '414-5162', 'asd', '2020-02-16 19:42:34', 1, 1),
(107, 'police', '463-7433', 'De #835-5913 : Hitler 128.4459991455, -1297.8000488282', '2020-02-17 20:35:07', 1, 0),
(108, 'police', '463-7433', 'De #463-7433 : asd', '2020-02-17 20:52:31', 1, 0),
(109, 'police', '463-7433', 'asd', '2020-02-17 20:52:31', 1, 1),
(110, 'police', '463-7433', 'De #463-7433 : asd 434.38827514648, -981.97973632812', '2020-02-17 20:53:31', 1, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `phone_users_contacts`
--

CREATE TABLE `phone_users_contacts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL,
  `number` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `display` varchar(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '-1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `entering` varchar(255) DEFAULT NULL,
  `exit` varchar(255) DEFAULT NULL,
  `inside` varchar(255) DEFAULT NULL,
  `outside` varchar(255) DEFAULT NULL,
  `ipls` varchar(255) DEFAULT '[]',
  `gateway` varchar(255) DEFAULT NULL,
  `is_single` int(11) DEFAULT NULL,
  `is_room` int(11) DEFAULT NULL,
  `is_gateway` int(11) DEFAULT NULL,
  `room_menu` varchar(255) DEFAULT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `properties`
--

INSERT INTO `properties` (`id`, `name`, `label`, `entering`, `exit`, `inside`, `outside`, `ipls`, `gateway`, `is_single`, `is_room`, `is_gateway`, `room_menu`, `price`) VALUES
(1, 'WhispymoundDrive', '2677 Whispymound Drive', '{\"y\":564.89,\"z\":182.959,\"x\":119.384}', '{\"x\":117.347,\"y\":559.506,\"z\":183.304}', '{\"y\":557.032,\"z\":183.301,\"x\":118.037}', '{\"y\":567.798,\"z\":182.131,\"x\":119.249}', '[]', NULL, 1, 1, 0, '{\"x\":118.748,\"y\":566.573,\"z\":175.697}', 1500000),
(2, 'NorthConkerAvenue2045', '2045 North Conker Avenue', '{\"x\":372.796,\"y\":428.327,\"z\":144.685}', '{\"x\":373.548,\"y\":422.982,\"z\":144.907},', '{\"y\":420.075,\"z\":145.904,\"x\":372.161}', '{\"x\":372.454,\"y\":432.886,\"z\":143.443}', '[]', NULL, 1, 1, 0, '{\"x\":377.349,\"y\":429.422,\"z\":137.3}', 1500000),
(3, 'RichardMajesticApt2', 'Richard Majestic, Apt 2', '{\"y\":-379.165,\"z\":37.961,\"x\":-936.363}', '{\"y\":-365.476,\"z\":113.274,\"x\":-913.097}', '{\"y\":-367.637,\"z\":113.274,\"x\":-918.022}', '{\"y\":-382.023,\"z\":37.961,\"x\":-943.626}', '[]', NULL, 1, 1, 0, '{\"x\":-927.554,\"y\":-377.744,\"z\":112.674}', 1700000),
(4, 'NorthConkerAvenue2044', '2044 North Conker Avenue', '{\"y\":440.8,\"z\":146.702,\"x\":346.964}', '{\"y\":437.456,\"z\":148.394,\"x\":341.683}', '{\"y\":435.626,\"z\":148.394,\"x\":339.595}', '{\"x\":350.535,\"y\":443.329,\"z\":145.764}', '[]', NULL, 1, 1, 0, '{\"x\":337.726,\"y\":436.985,\"z\":140.77}', 1500000),
(5, 'WildOatsDrive', '3655 Wild Oats Drive', '{\"y\":502.696,\"z\":136.421,\"x\":-176.003}', '{\"y\":497.817,\"z\":136.653,\"x\":-174.349}', '{\"y\":495.069,\"z\":136.666,\"x\":-173.331}', '{\"y\":506.412,\"z\":135.0664,\"x\":-177.927}', '[]', NULL, 1, 1, 0, '{\"x\":-174.725,\"y\":493.095,\"z\":129.043}', 1500000),
(6, 'HillcrestAvenue2862', '2862 Hillcrest Avenue', '{\"y\":596.58,\"z\":142.641,\"x\":-686.554}', '{\"y\":591.988,\"z\":144.392,\"x\":-681.728}', '{\"y\":590.608,\"z\":144.392,\"x\":-680.124}', '{\"y\":599.019,\"z\":142.059,\"x\":-689.492}', '[]', NULL, 1, 1, 0, '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', 1500000),
(7, 'LowEndApartment', 'Appartement de base', '{\"y\":-1078.735,\"z\":28.4031,\"x\":292.528}', '{\"y\":-1007.152,\"z\":-102.002,\"x\":265.845}', '{\"y\":-1002.802,\"z\":-100.008,\"x\":265.307}', '{\"y\":-1078.669,\"z\":28.401,\"x\":296.738}', '[]', NULL, 1, 1, 0, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', 562500),
(8, 'MadWayneThunder', '2113 Mad Wayne Thunder', '{\"y\":454.955,\"z\":96.462,\"x\":-1294.433}', '{\"x\":-1289.917,\"y\":449.541,\"z\":96.902}', '{\"y\":446.322,\"z\":96.899,\"x\":-1289.642}', '{\"y\":455.453,\"z\":96.517,\"x\":-1298.851}', '[]', NULL, 1, 1, 0, '{\"x\":-1287.306,\"y\":455.901,\"z\":89.294}', 1500000),
(9, 'HillcrestAvenue2874', '2874 Hillcrest Avenue', '{\"x\":-853.346,\"y\":696.678,\"z\":147.782}', '{\"y\":690.875,\"z\":151.86,\"x\":-859.961}', '{\"y\":688.361,\"z\":151.857,\"x\":-859.395}', '{\"y\":701.628,\"z\":147.773,\"x\":-855.007}', '[]', NULL, 1, 1, 0, '{\"x\":-858.543,\"y\":697.514,\"z\":144.253}', 1500000),
(10, 'HillcrestAvenue2868', '2868 Hillcrest Avenue', '{\"y\":620.494,\"z\":141.588,\"x\":-752.82}', '{\"y\":618.62,\"z\":143.153,\"x\":-759.317}', '{\"y\":617.629,\"z\":143.153,\"x\":-760.789}', '{\"y\":621.281,\"z\":141.254,\"x\":-750.919}', '[]', NULL, 1, 1, 0, '{\"x\":-762.504,\"y\":618.992,\"z\":135.53}', 1500000),
(11, 'TinselTowersApt12', 'Tinsel Towers, Apt 42', '{\"y\":37.025,\"z\":42.58,\"x\":-618.299}', '{\"y\":58.898,\"z\":97.2,\"x\":-603.301}', '{\"y\":58.941,\"z\":97.2,\"x\":-608.741}', '{\"y\":30.603,\"z\":42.524,\"x\":-620.017}', '[]', NULL, 1, 1, 0, '{\"x\":-622.173,\"y\":54.585,\"z\":96.599}', 1700000),
(12, 'MiltonDrive', 'Milton Drive', '{\"x\":-775.17,\"y\":312.01,\"z\":84.658}', NULL, NULL, '{\"x\":-775.346,\"y\":306.776,\"z\":84.7}', '[]', NULL, 0, 0, 1, NULL, 0),
(13, 'Modern1Apartment', 'Appartement Moderne 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_01_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.661,\"y\":327.672,\"z\":210.396}', 1300000),
(14, 'Modern2Apartment', 'Appartement Moderne 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_01_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.735,\"y\":326.757,\"z\":186.313}', 1300000),
(15, 'Modern3Apartment', 'Appartement Moderne 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_01_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.386,\"y\":330.782,\"z\":195.08}', 1300000),
(16, 'Mody1Apartment', 'Appartement Mode 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_02_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.615,\"y\":327.878,\"z\":210.396}', 1300000),
(17, 'Mody2Apartment', 'Appartement Mode 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_02_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.297,\"y\":327.092,\"z\":186.313}', 1300000),
(18, 'Mody3Apartment', 'Appartement Mode 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_02_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.303,\"y\":330.932,\"z\":195.085}', 1300000),
(19, 'Vibrant1Apartment', 'Appartement Vibrant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_03_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.885,\"y\":327.641,\"z\":210.396}', 1300000),
(20, 'Vibrant2Apartment', 'Appartement Vibrant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_03_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.607,\"y\":327.344,\"z\":186.313}', 1300000),
(21, 'Vibrant3Apartment', 'Appartement Vibrant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_03_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.525,\"y\":330.851,\"z\":195.085}', 1300000),
(22, 'Sharp1Apartment', 'Appartement Persan 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_04_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.527,\"y\":327.89,\"z\":210.396}', 1300000),
(23, 'Sharp2Apartment', 'Appartement Persan 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_04_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.642,\"y\":326.497,\"z\":186.313}', 1300000),
(24, 'Sharp3Apartment', 'Appartement Persan 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_04_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.503,\"y\":331.318,\"z\":195.085}', 1300000),
(25, 'Monochrome1Apartment', 'Appartement Monochrome 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_05_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.289,\"y\":328.086,\"z\":210.396}', 1300000),
(26, 'Monochrome2Apartment', 'Appartement Monochrome 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_05_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.692,\"y\":326.762,\"z\":186.313}', 1300000),
(27, 'Monochrome3Apartment', 'Appartement Monochrome 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_05_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.094,\"y\":330.976,\"z\":195.085}', 1300000),
(28, 'Seductive1Apartment', 'Appartement Séduisant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_06_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.263,\"y\":328.104,\"z\":210.396}', 1300000),
(29, 'Seductive2Apartment', 'Appartement Séduisant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_06_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.655,\"y\":326.611,\"z\":186.313}', 1300000),
(30, 'Seductive3Apartment', 'Appartement Séduisant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_06_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.3,\"y\":331.414,\"z\":195.085}', 1300000),
(31, 'Regal1Apartment', 'Appartement Régal 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_07_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.956,\"y\":328.257,\"z\":210.396}', 1300000),
(32, 'Regal2Apartment', 'Appartement Régal 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_07_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.545,\"y\":326.659,\"z\":186.313}', 1300000),
(33, 'Regal3Apartment', 'Appartement Régal 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_07_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.087,\"y\":331.429,\"z\":195.123}', 1300000),
(34, 'Aqua1Apartment', 'Appartement Aqua 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_08_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.187,\"y\":328.47,\"z\":210.396}', 1300000),
(35, 'Aqua2Apartment', 'Appartement Aqua 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_08_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.658,\"y\":326.563,\"z\":186.313}', 1300000),
(36, 'Aqua3Apartment', 'Appartement Aqua 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_08_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.287,\"y\":331.084,\"z\":195.086}', 1300000),
(37, 'IntegrityWay', '4 Integrity Way', '{\"x\":-47.804,\"y\":-585.867,\"z\":36.956}', NULL, NULL, '{\"x\":-54.178,\"y\":-583.762,\"z\":35.798}', '[]', NULL, 0, 0, 1, NULL, 0),
(38, 'IntegrityWay28', '4 Integrity Way - Apt 28', NULL, '{\"x\":-31.409,\"y\":-594.927,\"z\":79.03}', '{\"x\":-26.098,\"y\":-596.909,\"z\":79.03}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-11.923,\"y\":-597.083,\"z\":78.43}', 1700000),
(39, 'IntegrityWay30', '4 Integrity Way - Apt 30', NULL, '{\"x\":-17.702,\"y\":-588.524,\"z\":89.114}', '{\"x\":-16.21,\"y\":-582.569,\"z\":89.114}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-26.327,\"y\":-588.384,\"z\":89.123}', 1700000),
(40, 'DellPerroHeights', 'Dell Perro Heights', '{\"x\":-1447.06,\"y\":-538.28,\"z\":33.74}', NULL, NULL, '{\"x\":-1440.022,\"y\":-548.696,\"z\":33.74}', '[]', NULL, 0, 0, 1, NULL, 0),
(41, 'DellPerroHeightst4', 'Dell Perro Heights - Apt 28', NULL, '{\"x\":-1452.125,\"y\":-540.591,\"z\":73.044}', '{\"x\":-1455.435,\"y\":-535.79,\"z\":73.044}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1467.058,\"y\":-527.571,\"z\":72.443}', 1700000),
(42, 'DellPerroHeightst7', 'Dell Perro Heights - Apt 30', NULL, '{\"x\":-1451.562,\"y\":-523.535,\"z\":55.928}', '{\"x\":-1456.02,\"y\":-519.209,\"z\":55.929}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1457.026,\"y\":-530.219,\"z\":55.937}', 1700000);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rented_vehicles`
--

CREATE TABLE `rented_vehicles` (
  `vehicle` varchar(60) NOT NULL,
  `plate` varchar(12) NOT NULL,
  `player_name` varchar(255) NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `store` varchar(100) NOT NULL,
  `item` varchar(100) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `shops`
--

INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
(1, 'TwentyFourSeven', 'bread', 30),
(2, 'TwentyFourSeven', 'water', 15),
(3, 'RobsLiquor', 'bread', 30),
(4, 'RobsLiquor', 'water', 15),
(5, 'LTDgasoline', 'bread', 30),
(6, 'LTDgasoline', 'water', 15),
(7, 'LTDgasoline', 'phone', 1000),
(8, 'RobsLiquor', 'phone', 1000),
(9, 'TwentyFourSeven', 'phone', 1000);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `society_moneywash`
--

CREATE TABLE `society_moneywash` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) NOT NULL,
  `society` varchar(60) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `truck_inventory`
--

CREATE TABLE `truck_inventory` (
  `id` int(11) NOT NULL,
  `item` varchar(100) NOT NULL,
  `itemt` varchar(100) NOT NULL,
  `count` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `owned` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `twitter_accounts`
--

CREATE TABLE `twitter_accounts` (
  `id` int(11) NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `password` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `avatar_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `twitter_likes`
--

CREATE TABLE `twitter_likes` (
  `id` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `tweetId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `twitter_tweets`
--

CREATE TABLE `twitter_tweets` (
  `id` int(11) NOT NULL,
  `authorId` int(11) NOT NULL,
  `realUser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `skin` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `job` varchar(50) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT 0,
  `loadout` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `inventory` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `position` varchar(100) COLLATE utf8mb4_bin DEFAULT '{"x":-269.4,"y":-955.3,"z":31.2,"heading":205.8}',
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `phone_number` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_dead` tinyint(1) DEFAULT 0,
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `jail` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`identifier`, `license`, `money`, `bank`, `permission_level`, `group`, `skin`, `job`, `job_grade`, `loadout`, `inventory`, `position`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `phone_number`, `is_dead`, `last_property`, `jail`) VALUES
('Char2:110000136dd07fc', 'license:36e9b3231f0be771fdd1e7214ad841113d10bf6b', 0, 200, 0, 'user', '{\"moles_1\":0,\"eyebrows_2\":0,\"torso_2\":0,\"chest_1\":0,\"complexion_1\":0,\"beard_3\":0,\"beard_2\":0,\"sun_2\":0,\"watches_1\":-1,\"watches_2\":0,\"hair_color_2\":0,\"decals_1\":0,\"skin\":0,\"helmet_1\":-1,\"pants_2\":0,\"glasses_1\":0,\"hair_1\":0,\"torso_1\":0,\"beard_1\":0,\"decals_2\":0,\"pants_1\":0,\"makeup_1\":0,\"ears_2\":0,\"blush_2\":0,\"arms\":0,\"complexion_2\":0,\"chest_3\":0,\"bodyb_2\":0,\"hair_2\":0,\"ears_1\":-1,\"blush_3\":0,\"eye_color\":0,\"lipstick_2\":0,\"bproof_2\":0,\"eyebrows_1\":0,\"lipstick_1\":0,\"hair_color_1\":0,\"lipstick_4\":0,\"makeup_4\":0,\"helmet_2\":0,\"arms_2\":0,\"eyebrows_3\":0,\"mask_1\":0,\"makeup_3\":0,\"bracelets_2\":0,\"age_1\":0,\"age_2\":0,\"moles_2\":0,\"sun_1\":0,\"bproof_1\":0,\"blemishes_1\":0,\"blush_1\":0,\"tshirt_2\":0,\"tshirt_1\":0,\"makeup_2\":0,\"bodyb_1\":0,\"bags_1\":0,\"bags_2\":0,\"bracelets_1\":-1,\"face\":0,\"chain_2\":0,\"chest_2\":0,\"chain_1\":0,\"shoes_2\":0,\"sex\":0,\"blemishes_2\":0,\"eyebrows_4\":0,\"glasses_2\":0,\"shoes_1\":0,\"lipstick_3\":0,\"mask_2\":0,\"beard_4\":0}', 'unemployed', 0, '[]', '[]', '{\"z\":29.7,\"heading\":63.0,\"x\":-201.8,\"y\":-861.8}', 'Ddfghzj', 'Sfsafsadfda', '11.11.11', 'm', '190', '577-2870', 0, NULL, 0),
('steam:11000010acc835e', 'license:b40f7e504e72fc272f41b142d87b033203f75cb6', 50000, 100000, 0, 'user', '{\"mask_1\":0,\"eyebrows_2\":0,\"glasses_1\":0,\"complexion_2\":0,\"age_1\":0,\"ears_2\":0,\"moles_1\":0,\"shoes_1\":0,\"bracelets_1\":-1,\"beard_2\":0,\"lipstick_2\":0,\"hair_1\":0,\"blush_2\":0,\"lipstick_3\":0,\"torso_2\":0,\"beard_1\":0,\"chest_2\":0,\"glasses_2\":0,\"tshirt_2\":0,\"bodyb_2\":0,\"makeup_1\":0,\"chest_3\":0,\"watches_1\":-1,\"skin\":0,\"bracelets_2\":0,\"bproof_2\":0,\"lipstick_4\":0,\"makeup_3\":0,\"hair_2\":0,\"bproof_1\":0,\"eye_color\":0,\"decals_2\":0,\"watches_2\":0,\"helmet_2\":0,\"moles_2\":0,\"bags_1\":0,\"complexion_1\":0,\"blemishes_1\":0,\"lipstick_1\":0,\"eyebrows_4\":0,\"beard_3\":0,\"arms_2\":0,\"ears_1\":-1,\"pants_2\":0,\"arms\":0,\"chain_1\":0,\"bags_2\":0,\"blush_3\":0,\"pants_1\":0,\"torso_1\":0,\"eyebrows_1\":0,\"makeup_2\":0,\"age_2\":0,\"chain_2\":0,\"sun_2\":0,\"face\":0,\"decals_1\":0,\"shoes_2\":0,\"beard_4\":0,\"helmet_1\":-1,\"eyebrows_3\":0,\"hair_color_1\":0,\"tshirt_1\":0,\"blush_1\":0,\"hair_color_2\":0,\"makeup_4\":0,\"blemishes_2\":0,\"mask_2\":0,\"chest_1\":0,\"sun_1\":0,\"sex\":0,\"bodyb_1\":0}', 'unemployed', 0, '[]', '[]', '{\"heading\":205.79999999998,\"x\":-269.40000000002,\"y\":-955.30000000004,\"z\":31.199999999998}', 'Aasd', 'AADAD', '445245654', 'm', '200', '263-6196', 0, NULL, 0),
('steam:1100001102420a8', 'license:23168a2e4c0d065e742b9f7a4481374508ae22f8', 50000, 100000, 0, 'user', '{\"age_1\":0,\"beard_1\":0,\"beard_4\":0,\"decals_2\":0,\"mask_2\":0,\"bracelets_1\":-1,\"chain_1\":0,\"moles_2\":0,\"eyebrows_3\":0,\"arms\":1,\"beard_3\":0,\"helmet_1\":-1,\"complexion_2\":0,\"hair_color_2\":0,\"blush_2\":0,\"sun_1\":0,\"decals_1\":0,\"skin\":2,\"chest_2\":0,\"shoes_2\":0,\"complexion_1\":0,\"arms_2\":0,\"moles_1\":0,\"age_2\":0,\"beard_2\":0,\"watches_2\":0,\"makeup_3\":0,\"eyebrows_4\":0,\"shoes_1\":1,\"makeup_4\":0,\"bracelets_2\":0,\"sun_2\":0,\"torso_1\":6,\"glasses_1\":0,\"bodyb_2\":0,\"hair_color_1\":0,\"tshirt_2\":0,\"blush_3\":0,\"watches_1\":-1,\"torso_2\":0,\"chain_2\":0,\"bags_1\":0,\"pants_1\":5,\"tshirt_1\":0,\"lipstick_2\":0,\"blemishes_2\":0,\"hair_2\":0,\"bproof_2\":0,\"bproof_1\":0,\"pants_2\":2,\"hair_1\":3,\"eyebrows_2\":0,\"makeup_1\":0,\"sex\":0,\"chest_1\":0,\"makeup_2\":0,\"chest_3\":0,\"helmet_2\":0,\"bags_2\":0,\"blemishes_1\":0,\"glasses_2\":0,\"blush_1\":0,\"face\":0,\"eye_color\":0,\"ears_2\":0,\"lipstick_4\":0,\"lipstick_3\":0,\"bodyb_1\":0,\"mask_1\":0,\"eyebrows_1\":0,\"ears_1\":-1,\"lipstick_1\":0}', 'fueler', 0, '[]', '[]', '{\"y\":-1276.3,\"z\":30.5,\"heading\":208.2,\"x\":-263.2}', 'Kevin', 'Hilton', '20060113', 'm', '180', '716-7826', 0, NULL, 0),
('steam:11000011300960a', 'license:b40f7e504e72fc272f41b142d87b033203f75cb6', 49976, 197500, 0, 'user', '{\"mask_1\":0,\"eyebrows_2\":0,\"glasses_1\":0,\"complexion_2\":0,\"decals_1\":0,\"ears_2\":0,\"moles_1\":0,\"shoes_1\":10,\"bracelets_1\":-1,\"tshirt_1\":10,\"lipstick_2\":0,\"hair_1\":10,\"blush_2\":0,\"lipstick_3\":0,\"torso_2\":2,\"beard_1\":0,\"chest_2\":0,\"glasses_2\":0,\"tshirt_2\":2,\"bodyb_2\":0,\"makeup_1\":0,\"chest_3\":0,\"watches_1\":-1,\"skin\":0,\"bracelets_2\":0,\"bproof_2\":0,\"face\":0,\"makeup_3\":0,\"makeup_4\":0,\"bproof_1\":0,\"eye_color\":0,\"decals_2\":0,\"watches_2\":0,\"helmet_2\":0,\"moles_2\":0,\"bags_1\":0,\"complexion_1\":0,\"blemishes_1\":0,\"lipstick_1\":0,\"eyebrows_4\":0,\"beard_3\":0,\"arms_2\":0,\"blemishes_2\":0,\"pants_2\":0,\"arms\":8,\"chain_1\":0,\"sun_2\":0,\"blush_3\":0,\"pants_1\":10,\"torso_1\":72,\"sun_1\":0,\"makeup_2\":0,\"eyebrows_1\":0,\"chain_2\":0,\"beard_2\":0,\"lipstick_4\":0,\"bags_2\":0,\"age_1\":0,\"beard_4\":0,\"ears_1\":-1,\"eyebrows_3\":0,\"hair_color_1\":0,\"age_2\":0,\"blush_1\":0,\"hair_color_2\":0,\"shoes_2\":0,\"sex\":0,\"hair_2\":0,\"helmet_1\":-1,\"chest_1\":0,\"mask_2\":0,\"bodyb_1\":0}', 'cardealer', 3, '[{\"label\":\"Schläger\",\"tintIndex\":0,\"name\":\"WEAPON_BAT\",\"components\":[],\"ammo\":0}]', '[]', '{\"y\":-1101.5,\"z\":26.4,\"heading\":143.8,\"x\":-33.6}', 'Lukas', 'Jac', '2000-02-27', 'm', '200', '414-5162', 0, NULL, 0),
('steam:110000114cf3e89', 'license:5589864dee66a5517bd42a049f8c2afa4bc02bb1', 49842, 377700, 0, 'user', '{\"bags_2\":0,\"watches_1\":-1,\"beard_1\":0,\"lipstick_1\":0,\"sex\":0,\"hair_2\":0,\"face\":3,\"makeup_2\":0,\"makeup_3\":0,\"glasses_1\":3,\"beard_2\":0,\"makeup_4\":0,\"makeup_1\":0,\"bags_1\":0,\"helmet_2\":0,\"eyebrows_4\":0,\"beard_3\":0,\"torso_1\":76,\"moles_2\":0,\"eye_color\":0,\"lipstick_4\":0,\"eyebrows_3\":0,\"shoes_2\":0,\"bodyb_1\":0,\"bodyb_2\":0,\"glasses_2\":0,\"blush_3\":0,\"age_1\":0,\"tshirt_1\":11,\"torso_2\":1,\"sun_1\":0,\"age_2\":0,\"pants_2\":0,\"hair_color_1\":0,\"pants_1\":10,\"chest_1\":0,\"mask_2\":0,\"chest_3\":0,\"ears_1\":-1,\"lipstick_2\":0,\"helmet_1\":10,\"blush_2\":0,\"arms\":6,\"watches_2\":0,\"chain_1\":0,\"tshirt_2\":0,\"eyebrows_2\":0,\"bracelets_1\":-1,\"sun_2\":0,\"eyebrows_1\":0,\"hair_color_2\":0,\"complexion_1\":0,\"blemishes_1\":0,\"arms_2\":0,\"bproof_1\":0,\"beard_4\":0,\"hair_1\":3,\"decals_1\":0,\"decals_2\":0,\"lipstick_3\":0,\"complexion_2\":0,\"blush_1\":0,\"bracelets_2\":0,\"shoes_1\":10,\"bproof_2\":0,\"mask_1\":0,\"blemishes_2\":0,\"ears_2\":0,\"moles_1\":0,\"chest_2\":0,\"skin\":3,\"chain_2\":0}', 'police', 4, '[{\"name\":\"WEAPON_APPISTOL\",\"tintIndex\":0,\"ammo\":100,\"components\":[\"clip_default\"],\"label\":\"AP Pistole\"},{\"name\":\"WEAPON_SMG\",\"tintIndex\":0,\"ammo\":100,\"components\":[\"clip_default\"],\"label\":\"SMG\"},{\"name\":\"WEAPON_STUNGUN\",\"tintIndex\":0,\"ammo\":100,\"components\":[],\"label\":\"Taser\"},{\"name\":\"WEAPON_FLASHLIGHT\",\"tintIndex\":0,\"ammo\":0,\"components\":[],\"label\":\"Taschenlampe\"},{\"name\":\"WEAPON_NIGHTSTICK\",\"tintIndex\":0,\"ammo\":0,\"components\":[],\"label\":\"Schlagstock\"}]', '[]', '{\"z\":30.7,\"heading\":113.0,\"x\":453.9,\"y\":-992.0}', 'Louis', 'Jackson', '20000402', 'm', '200', '463-7433', 0, NULL, 0),
('steam:1100001195c1d46', 'license:a45130e89adf94dd4f6ba5d1ac699dba9a02aedc', 149700, 0, 0, 'user', '{\"tshirt_1\":15,\"eyebrows_2\":0,\"torso_2\":0,\"chest_1\":0,\"complexion_1\":0,\"beard_3\":0,\"beard_2\":0,\"makeup_2\":0,\"chain_1\":0,\"watches_2\":0,\"mask_2\":0,\"decals_1\":0,\"skin\":0,\"helmet_1\":-1,\"pants_2\":7,\"glasses_1\":0,\"hair_1\":2,\"torso_1\":1,\"beard_1\":0,\"decals_2\":0,\"pants_1\":9,\"makeup_1\":0,\"ears_2\":0,\"blush_2\":0,\"arms\":0,\"complexion_2\":0,\"chest_3\":0,\"bodyb_2\":0,\"hair_2\":0,\"ears_1\":-1,\"blush_3\":0,\"eye_color\":0,\"lipstick_2\":0,\"bproof_2\":0,\"age_1\":0,\"lipstick_1\":0,\"hair_color_1\":6,\"lipstick_4\":0,\"moles_2\":0,\"sun_1\":0,\"arms_2\":0,\"blemishes_1\":0,\"mask_1\":0,\"makeup_3\":0,\"bracelets_2\":0,\"shoes_2\":0,\"age_2\":0,\"bags_2\":0,\"makeup_4\":0,\"bproof_1\":0,\"beard_4\":0,\"blush_1\":0,\"tshirt_2\":0,\"watches_1\":-1,\"helmet_2\":0,\"bodyb_1\":0,\"bags_1\":0,\"sun_2\":0,\"bracelets_1\":-1,\"eyebrows_4\":0,\"moles_1\":0,\"chest_2\":0,\"eyebrows_1\":0,\"face\":0,\"sex\":0,\"blemishes_2\":0,\"hair_color_2\":0,\"eyebrows_3\":0,\"shoes_1\":1,\"lipstick_3\":0,\"chain_2\":0,\"glasses_2\":0}', 'miner', 0, '[]', '[]', '{\"z\":30.6,\"heading\":311.3,\"x\":238.7,\"y\":-1390.7}', 'Kalle', 'Kuschinsky', '25.5.1990', 'm', '186', '200-9432', 0, NULL, 0),
('steam:11000011a0c3cab', 'license:1e0355fef162708129f7a4615450418ec91fdc50', 2000, 18800, 0, 'user', '{\"age_1\":0,\"beard_1\":7,\"beard_4\":0,\"decals_2\":0,\"mask_2\":0,\"bracelets_1\":-1,\"chain_1\":0,\"moles_2\":0,\"eyebrows_3\":16,\"arms\":0,\"beard_3\":0,\"helmet_1\":-1,\"complexion_2\":0,\"hair_color_2\":0,\"blush_2\":0,\"sun_1\":0,\"decals_1\":0,\"skin\":4,\"chest_2\":0,\"shoes_2\":0,\"complexion_1\":0,\"arms_2\":0,\"moles_1\":0,\"age_2\":0,\"eyebrows_1\":0,\"chest_1\":0,\"lipstick_4\":0,\"eyebrows_4\":18,\"shoes_1\":7,\"makeup_4\":0,\"bracelets_2\":0,\"sun_2\":0,\"torso_1\":167,\"glasses_1\":0,\"bodyb_2\":0,\"hair_color_1\":0,\"chest_3\":0,\"blush_3\":0,\"watches_1\":-1,\"torso_2\":14,\"chain_2\":0,\"bags_1\":0,\"glasses_2\":0,\"tshirt_1\":0,\"sex\":0,\"blemishes_2\":0,\"hair_2\":0,\"makeup_1\":0,\"bproof_1\":0,\"tshirt_2\":2,\"pants_1\":27,\"eyebrows_2\":10,\"bags_2\":0,\"blush_1\":0,\"pants_2\":3,\"bproof_2\":0,\"lipstick_1\":0,\"helmet_2\":0,\"lipstick_2\":0,\"lipstick_3\":0,\"watches_2\":0,\"hair_1\":3,\"face\":4,\"blemishes_1\":0,\"ears_2\":0,\"makeup_2\":0,\"beard_2\":10,\"bodyb_1\":0,\"makeup_3\":0,\"mask_1\":0,\"ears_1\":-1,\"eye_color\":0}', 'fisherman', 0, '[]', '[]', '{\"y\":-1359.2,\"z\":29.0,\"heading\":89.6,\"x\":5.6}', 'Sebastian', 'New', '2002-09-17', 'm', '189', '481-4044', 0, NULL, 0),
('steam:11000011b642819', 'license:94a51db230b4dad42be80f1f1ff16f0004bd61a8', 49500, 102800, 0, 'user', '{\"blemishes_2\":0,\"hair_2\":0,\"makeup_1\":0,\"sun_1\":0,\"watches_1\":-1,\"ears_2\":0,\"blush_2\":0,\"chest_1\":0,\"decals_1\":0,\"helmet_2\":0,\"bodyb_1\":0,\"shoes_1\":26,\"bodyb_2\":0,\"hair_1\":21,\"blush_1\":0,\"arms\":15,\"hair_color_2\":0,\"age_1\":0,\"hair_color_1\":0,\"bags_1\":0,\"shoes_2\":7,\"pants_2\":5,\"helmet_1\":56,\"chest_2\":0,\"chain_1\":50,\"eyebrows_3\":0,\"makeup_2\":0,\"sun_2\":0,\"eyebrows_1\":0,\"makeup_3\":0,\"lipstick_3\":0,\"bproof_1\":0,\"eye_color\":0,\"complexion_2\":0,\"moles_1\":0,\"skin\":3,\"moles_2\":0,\"torso_1\":15,\"torso_2\":0,\"chain_2\":0,\"bproof_2\":0,\"age_2\":0,\"lipstick_1\":0,\"blush_3\":0,\"beard_2\":0,\"eyebrows_4\":0,\"mask_2\":0,\"makeup_4\":0,\"bracelets_2\":0,\"beard_4\":0,\"tshirt_2\":0,\"complexion_1\":0,\"mask_1\":0,\"bracelets_1\":-1,\"watches_2\":0,\"decals_2\":0,\"beard_3\":0,\"face\":0,\"eyebrows_2\":0,\"tshirt_1\":15,\"lipstick_2\":0,\"lipstick_4\":0,\"beard_1\":0,\"bags_2\":0,\"glasses_2\":0,\"ears_1\":-1,\"chest_3\":0,\"sex\":0,\"arms_2\":0,\"glasses_1\":5,\"pants_1\":42,\"blemishes_1\":0}', 'unemployed', 0, '[]', '[]', '{\"z\":30.7,\"y\":-985.1,\"x\":447.5,\"heading\":259.7}', 'Louis', 'Black', '04.05.1997', 'm', '187', '835-5913', 0, NULL, 0),
('steam:110000136dd07fc', 'license:36e9b3231f0be771fdd1e7214ad841113d10bf6b', 50000, 100000, 0, 'user', '{\"tshirt_1\":22,\"eyebrows_2\":0,\"torso_2\":1,\"chest_1\":0,\"complexion_1\":0,\"beard_3\":0,\"beard_2\":0,\"makeup_2\":0,\"watches_1\":-1,\"watches_2\":0,\"glasses_2\":0,\"decals_1\":0,\"skin\":8,\"helmet_1\":-1,\"pants_2\":5,\"glasses_1\":0,\"hair_1\":21,\"torso_1\":11,\"bproof_1\":0,\"decals_2\":0,\"beard_4\":0,\"makeup_1\":0,\"ears_2\":0,\"blush_2\":0,\"arms\":1,\"complexion_2\":0,\"chest_3\":0,\"bodyb_2\":0,\"hair_2\":0,\"ears_1\":-1,\"blush_3\":0,\"eye_color\":0,\"lipstick_2\":0,\"bproof_2\":0,\"eyebrows_1\":0,\"lipstick_1\":0,\"hair_color_1\":0,\"lipstick_4\":0,\"makeup_4\":0,\"sun_1\":0,\"arms_2\":0,\"blemishes_1\":0,\"chain_2\":0,\"makeup_3\":0,\"bracelets_2\":0,\"mask_1\":0,\"age_2\":0,\"moles_2\":0,\"sun_2\":0,\"eyebrows_4\":0,\"shoes_2\":0,\"blush_1\":0,\"tshirt_2\":4,\"bags_2\":0,\"moles_1\":0,\"bodyb_1\":0,\"bags_1\":0,\"age_1\":0,\"bracelets_1\":-1,\"face\":0,\"pants_1\":24,\"chest_2\":0,\"mask_2\":0,\"eyebrows_3\":0,\"sex\":0,\"blemishes_2\":0,\"helmet_2\":0,\"chain_1\":0,\"shoes_1\":10,\"lipstick_3\":0,\"beard_1\":0,\"hair_color_2\":0}', 'unemployed', 0, '[]', '[]', '{\"z\":23.0,\"heading\":266.6,\"x\":-227.8,\"y\":-1125.3}', 'Dennis', 'Adams', '20.09.2000', 'm', '190', '569-0285', 0, NULL, 0),
('steam:11000013f5799ba', 'license:8e2f903bceb862140e5cf8d4f92fbc90a0219ca8', 50000, 100000, 0, 'user', '{\"age_1\":0,\"beard_1\":0,\"beard_4\":0,\"decals_2\":0,\"mask_2\":0,\"bracelets_1\":-1,\"chain_1\":0,\"moles_2\":0,\"eyebrows_3\":0,\"arms\":0,\"beard_3\":0,\"helmet_1\":-1,\"complexion_2\":0,\"hair_color_2\":0,\"blush_2\":0,\"sun_1\":0,\"decals_1\":0,\"skin\":3,\"chest_2\":0,\"shoes_2\":0,\"complexion_1\":0,\"arms_2\":0,\"moles_1\":0,\"age_2\":0,\"eyebrows_1\":0,\"chest_1\":0,\"makeup_3\":0,\"eyebrows_4\":0,\"shoes_1\":0,\"makeup_4\":0,\"bracelets_2\":0,\"sun_2\":0,\"torso_1\":0,\"glasses_1\":0,\"bodyb_2\":0,\"hair_color_1\":0,\"tshirt_2\":0,\"blush_3\":0,\"watches_1\":-1,\"torso_2\":0,\"chain_2\":0,\"bags_1\":0,\"pants_1\":0,\"tshirt_1\":0,\"lipstick_2\":0,\"blemishes_2\":0,\"hair_2\":0,\"makeup_1\":0,\"bproof_1\":0,\"mask_1\":0,\"sex\":0,\"bags_2\":0,\"watches_2\":0,\"eyebrows_2\":0,\"bproof_2\":0,\"glasses_2\":0,\"chest_3\":0,\"helmet_2\":0,\"pants_2\":0,\"lipstick_3\":0,\"lipstick_4\":0,\"blush_1\":0,\"face\":2,\"makeup_2\":0,\"ears_2\":0,\"hair_1\":0,\"beard_2\":0,\"bodyb_1\":0,\"lipstick_1\":0,\"blemishes_1\":0,\"ears_1\":-1,\"eye_color\":0}', 'reporter', 0, '[]', '[]', '{\"y\":-955.30000000004,\"z\":31.199999999998,\"heading\":205.79999999998,\"x\":-269.40000000002}', 'John', 'Englisch', '1990.10.10', 'm', '180', '792-6224', 0, NULL, 0),
('steam:11000013f9a232d', 'license:3fcea2dabdd59400ff53007ef41fb69a69049867', 50000, 100200, 0, 'user', '{\"bags_2\":0,\"watches_1\":-1,\"skin\":0,\"lipstick_1\":0,\"sex\":0,\"hair_2\":0,\"face\":0,\"makeup_2\":0,\"makeup_3\":0,\"glasses_1\":0,\"beard_2\":0,\"makeup_4\":0,\"decals_2\":0,\"arms\":0,\"helmet_2\":0,\"eyebrows_4\":0,\"beard_3\":0,\"torso_1\":22,\"moles_2\":0,\"eye_color\":0,\"lipstick_4\":0,\"eyebrows_3\":0,\"shoes_2\":0,\"bodyb_1\":0,\"bodyb_2\":0,\"glasses_2\":0,\"blush_3\":0,\"chain_1\":0,\"tshirt_1\":15,\"torso_2\":2,\"sun_1\":0,\"age_2\":0,\"pants_2\":0,\"hair_color_1\":0,\"chest_2\":0,\"chest_1\":0,\"mask_2\":0,\"decals_1\":0,\"ears_1\":-1,\"lipstick_2\":0,\"helmet_1\":-1,\"lipstick_3\":0,\"bracelets_1\":-1,\"watches_2\":0,\"chest_3\":0,\"tshirt_2\":0,\"pants_1\":0,\"beard_1\":0,\"sun_2\":0,\"eyebrows_1\":0,\"age_1\":0,\"complexion_1\":0,\"hair_color_2\":0,\"arms_2\":0,\"bproof_1\":0,\"beard_4\":0,\"hair_1\":0,\"mask_1\":0,\"ears_2\":0,\"bags_1\":0,\"complexion_2\":0,\"blush_1\":0,\"bracelets_2\":0,\"shoes_1\":0,\"makeup_1\":0,\"blush_2\":0,\"blemishes_2\":0,\"bproof_2\":0,\"moles_1\":0,\"blemishes_1\":0,\"eyebrows_2\":0,\"chain_2\":0}', 'unemployed', 0, '[]', '[]', '{\"heading\":158.5,\"x\":-237.5,\"y\":-987.9,\"z\":28.8}', 'Mr', 'Hitman', '13.09.1990', 'm', '200', '132-1522', 1, NULL, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_accounts`
--

CREATE TABLE `user_accounts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(22) NOT NULL,
  `name` varchar(50) NOT NULL,
  `money` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `user_accounts`
--

INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
(1, 'steam:110000114cf3e89', 'black_money', 0),
(2, 'steam:110000110b3ac66', 'black_money', 0),
(3, 'Char2:110000114cf3e89', 'black_money', 0),
(5, 'Char2:11000011300960a', 'black_money', 0),
(6, 'steam:11000011300960a', 'black_money', 0),
(7, 'steam:11000010acc835e', 'black_money', 0),
(8, 'steam:11000013f9a232d', 'black_money', 0),
(9, 'steam:11000011b642819', 'black_money', 0),
(10, 'steam:1100001102420a8', 'black_money', 0),
(11, 'steam:11000013f5799ba', 'black_money', 0),
(12, 'steam:11000011a0c3cab', 'black_money', 0),
(13, 'steam:110000136dd07fc', 'black_money', 0),
(14, 'Char2:110000136dd07fc', 'black_money', 0),
(15, 'steam:1100001195c1d46', 'black_money', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_lastcharacter`
--

CREATE TABLE `user_lastcharacter` (
  `steamid` varchar(255) NOT NULL,
  `charid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `user_lastcharacter`
--

INSERT INTO `user_lastcharacter` (`steamid`, `charid`) VALUES
('steam:110000114cf3e89', 1),
('steam:110000110b3ac66', 1),
('steam:11000011300960a', 1),
('steam:11000010acc835e', 1),
('steam:11000013f9a232d', 1),
('steam:11000011b642819', 1),
('steam:1100001102420a8', 1),
('steam:11000013f5799ba', 1),
('steam:11000011a0c3cab', 1),
('steam:110000136dd07fc', 1),
('steam:1100001195c1d46', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_licenses`
--

CREATE TABLE `user_licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(60) NOT NULL,
  `owner` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `vehicles`
--

CREATE TABLE `vehicles` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `vehicles`
--

INSERT INTO `vehicles` (`name`, `model`, `price`, `category`) VALUES
('Lamborghini', '18performante', 400000, 'lamborghini'),
('63LB', '63lb', 260000, 'mercedes'),
('AMG C63', 'c63coupe', 230000, 'mercedes'),
('Bentley', 'CONTSS18', 450000, 'bentley'),
('E Klasse', 'e63amg', 320000, 'mercedes'),
('Z4GT3', 'e89', 130000, 'bmw'),
('Superfast812', 'ferrarie812', 360000, 'ferrari'),
('G Klasse', 'g65amg', 250000, 'mercedes'),
('Mustang', 'gt350r', 160000, 'ford'),
('MacanGTS', 'mcgts', 300000, 'porsche'),
('RS7', 'rmodrs7', 360000, 'audi'),
('TTRS', 'ttrs', 180000, 'audi');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `vehicle_categories`
--

CREATE TABLE `vehicle_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `vehicle_categories`
--

INSERT INTO `vehicle_categories` (`name`, `label`) VALUES
('audi', 'AUDI'),
('bentley', 'Bentley'),
('bmw', 'BMW'),
('dodge', 'Dodge'),
('ferrari', 'Ferrari'),
('ford', 'Ford'),
('lamborghini', 'Lamborghini'),
('mercedes', 'Mercedes'),
('porsche', 'Porsche');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `vehicle_sold`
--

CREATE TABLE `vehicle_sold` (
  `client` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `plate` varchar(50) NOT NULL,
  `soldby` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Daten für Tabelle `vehicle_sold`
--

INSERT INTO `vehicle_sold` (`client`, `model`, `plate`, `soldby`, `date`) VALUES
('Test Peter', 'Z4GT3', 'CPOA60', 'Lukas Jac', '2020-02-16 13:06'),
('Test Peterr', 'Z4GT3', 'KNVP55', 'Lukas Jac', '2020-02-16 13:11'),
('Sebastian New', 'TTRS', 'WOBS00', 'Lukas Jac', '2020-02-17 21:08'),
('Lukas Jac', 'Z4GT3', 'YBLM73', 'Test Peter', '2020-02-16 13:02');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `addon_account`
--
ALTER TABLE `addon_account`
  ADD PRIMARY KEY (`name`) USING BTREE;

--
-- Indizes für die Tabelle `addon_account_data`
--
ALTER TABLE `addon_account_data`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `index_addon_account_data_account_name_owner` (`account_name`,`owner`) USING BTREE,
  ADD KEY `index_addon_account_data_account_name` (`account_name`) USING BTREE;

--
-- Indizes für die Tabelle `addon_inventory`
--
ALTER TABLE `addon_inventory`
  ADD PRIMARY KEY (`name`) USING BTREE;

--
-- Indizes für die Tabelle `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `index_addon_inventory_items_inventory_name_name` (`inventory_name`,`name`) USING BTREE,
  ADD KEY `index_addon_inventory_items_inventory_name_name_owner` (`inventory_name`,`name`,`owner`) USING BTREE,
  ADD KEY `index_addon_inventory_inventory_name` (`inventory_name`) USING BTREE;

--
-- Indizes für die Tabelle `billing`
--
ALTER TABLE `billing`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indizes für die Tabelle `cardealer_vehicles`
--
ALTER TABLE `cardealer_vehicles`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indizes für die Tabelle `characters`
--
ALTER TABLE `characters`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indizes für die Tabelle `datastore`
--
ALTER TABLE `datastore`
  ADD PRIMARY KEY (`name`) USING BTREE;

--
-- Indizes für die Tabelle `datastore_data`
--
ALTER TABLE `datastore_data`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `index_datastore_data_name_owner` (`name`,`owner`) USING BTREE,
  ADD KEY `index_datastore_data_name` (`name`) USING BTREE;

--
-- Indizes für die Tabelle `fine_types`
--
ALTER TABLE `fine_types`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indizes für die Tabelle `import_vehicles`
--
ALTER TABLE `import_vehicles`
  ADD PRIMARY KEY (`model`);

--
-- Indizes für die Tabelle `import_vehicle_categories`
--
ALTER TABLE `import_vehicle_categories`
  ADD PRIMARY KEY (`name`);

--
-- Indizes für die Tabelle `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`name`) USING BTREE;

--
-- Indizes für die Tabelle `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`name`) USING BTREE;

--
-- Indizes für die Tabelle `job_grades`
--
ALTER TABLE `job_grades`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indizes für die Tabelle `licenses`
--
ALTER TABLE `licenses`
  ADD PRIMARY KEY (`type`) USING BTREE;

--
-- Indizes für die Tabelle `owned_properties`
--
ALTER TABLE `owned_properties`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indizes für die Tabelle `owned_vehicles`
--
ALTER TABLE `owned_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indizes für die Tabelle `phone_app_chat`
--
ALTER TABLE `phone_app_chat`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indizes für die Tabelle `phone_calls`
--
ALTER TABLE `phone_calls`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indizes für die Tabelle `phone_messages`
--
ALTER TABLE `phone_messages`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indizes für die Tabelle `phone_users_contacts`
--
ALTER TABLE `phone_users_contacts`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indizes für die Tabelle `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indizes für die Tabelle `rented_vehicles`
--
ALTER TABLE `rented_vehicles`
  ADD PRIMARY KEY (`plate`) USING BTREE;

--
-- Indizes für die Tabelle `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indizes für die Tabelle `society_moneywash`
--
ALTER TABLE `society_moneywash`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indizes für die Tabelle `truck_inventory`
--
ALTER TABLE `truck_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item` (`item`,`plate`);

--
-- Indizes für die Tabelle `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indizes für die Tabelle `twitter_likes`
--
ALTER TABLE `twitter_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_twitter_likes_twitter_accounts` (`authorId`),
  ADD KEY `FK_twitter_likes_twitter_tweets` (`tweetId`);

--
-- Indizes für die Tabelle `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_twitter_tweets_twitter_accounts` (`authorId`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`identifier`) USING BTREE;

--
-- Indizes für die Tabelle `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indizes für die Tabelle `user_licenses`
--
ALTER TABLE `user_licenses`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indizes für die Tabelle `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`model`) USING BTREE;

--
-- Indizes für die Tabelle `vehicle_categories`
--
ALTER TABLE `vehicle_categories`
  ADD PRIMARY KEY (`name`) USING BTREE;

--
-- Indizes für die Tabelle `vehicle_sold`
--
ALTER TABLE `vehicle_sold`
  ADD PRIMARY KEY (`plate`) USING BTREE;

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `addon_account_data`
--
ALTER TABLE `addon_account_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT für Tabelle `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `billing`
--
ALTER TABLE `billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT für Tabelle `cardealer_vehicles`
--
ALTER TABLE `cardealer_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT für Tabelle `characters`
--
ALTER TABLE `characters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT für Tabelle `datastore_data`
--
ALTER TABLE `datastore_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT für Tabelle `fine_types`
--
ALTER TABLE `fine_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT für Tabelle `job_grades`
--
ALTER TABLE `job_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT für Tabelle `owned_properties`
--
ALTER TABLE `owned_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `phone_app_chat`
--
ALTER TABLE `phone_app_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT für Tabelle `phone_calls`
--
ALTER TABLE `phone_calls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;

--
-- AUTO_INCREMENT für Tabelle `phone_messages`
--
ALTER TABLE `phone_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT für Tabelle `phone_users_contacts`
--
ALTER TABLE `phone_users_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT für Tabelle `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT für Tabelle `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT für Tabelle `society_moneywash`
--
ALTER TABLE `society_moneywash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `truck_inventory`
--
ALTER TABLE `truck_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT für Tabelle `twitter_likes`
--
ALTER TABLE `twitter_likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT für Tabelle `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT für Tabelle `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT für Tabelle `user_licenses`
--
ALTER TABLE `user_licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `twitter_likes`
--
ALTER TABLE `twitter_likes`
  ADD CONSTRAINT `FK_twitter_likes_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`),
  ADD CONSTRAINT `FK_twitter_likes_twitter_tweets` FOREIGN KEY (`tweetId`) REFERENCES `twitter_tweets` (`id`) ON DELETE CASCADE;

--
-- Constraints der Tabelle `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  ADD CONSTRAINT `FK_twitter_tweets_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
