/*
 Navicat Premium Data Transfer

 Source Server         : fivem
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : essentialmode

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 16/02/2020 12:08:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for addon_account
-- ----------------------------
DROP TABLE IF EXISTS `addon_account`;
CREATE TABLE `addon_account`  (
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `shared` int(0) NOT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of addon_account
-- ----------------------------
INSERT INTO `addon_account` VALUES ('property_black_money', 'Argent Sale Propriété', 0);
INSERT INTO `addon_account` VALUES ('society_ambulance', 'Ambulance', 1);
INSERT INTO `addon_account` VALUES ('society_cardealer', 'Händler', 1);
INSERT INTO `addon_account` VALUES ('society_police', 'Polizei', 1);

-- ----------------------------
-- Table structure for addon_account_data
-- ----------------------------
DROP TABLE IF EXISTS `addon_account_data`;
CREATE TABLE `addon_account_data`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `money` int(0) NOT NULL,
  `owner` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_addon_account_data_account_name_owner`(`account_name`, `owner`) USING BTREE,
  INDEX `index_addon_account_data_account_name`(`account_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of addon_account_data
-- ----------------------------
INSERT INTO `addon_account_data` VALUES (1, 'society_cardealer', 0, NULL);
INSERT INTO `addon_account_data` VALUES (2, 'society_police', 0, NULL);
INSERT INTO `addon_account_data` VALUES (3, 'society_ambulance', 0, NULL);
INSERT INTO `addon_account_data` VALUES (4, 'property_black_money', 0, 'steam:110000114cf3e89');
INSERT INTO `addon_account_data` VALUES (5, 'property_black_money', 0, 'steam:11000011300960a');

-- ----------------------------
-- Table structure for addon_inventory
-- ----------------------------
DROP TABLE IF EXISTS `addon_inventory`;
CREATE TABLE `addon_inventory`  (
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `shared` int(0) NOT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of addon_inventory
-- ----------------------------
INSERT INTO `addon_inventory` VALUES ('property', 'Propriété', 0);
INSERT INTO `addon_inventory` VALUES ('society_ambulance', 'Ambulance', 1);
INSERT INTO `addon_inventory` VALUES ('society_cardealer', 'Händler', 1);
INSERT INTO `addon_inventory` VALUES ('society_police', 'Polizei', 1);

-- ----------------------------
-- Table structure for addon_inventory_items
-- ----------------------------
DROP TABLE IF EXISTS `addon_inventory_items`;
CREATE TABLE `addon_inventory_items`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `inventory_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `count` int(0) NOT NULL,
  `owner` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_addon_inventory_items_inventory_name_name`(`inventory_name`, `name`) USING BTREE,
  INDEX `index_addon_inventory_items_inventory_name_name_owner`(`inventory_name`, `name`, `owner`) USING BTREE,
  INDEX `index_addon_inventory_inventory_name`(`inventory_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for billing
-- ----------------------------
DROP TABLE IF EXISTS `billing`;
CREATE TABLE `billing`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `sender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `target_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `target` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `amount` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cardealer_vehicles
-- ----------------------------
DROP TABLE IF EXISTS `cardealer_vehicles`;
CREATE TABLE `cardealer_vehicles`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `vehicle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `price` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for characters
-- ----------------------------
DROP TABLE IF EXISTS `characters`;
CREATE TABLE `characters`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `firstname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `dateofbirth` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `sex` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'M',
  `height` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of characters
-- ----------------------------
INSERT INTO `characters` VALUES (1, 'steam:110000114cf3e89', 'Test', 'Test', '20000402', 'm', '200');
INSERT INTO `characters` VALUES (2, 'steam:110000114cf3e89', 'Louis', 'Jackson', '20000402', 'm', '200');
INSERT INTO `characters` VALUES (3, 'steam:110000114cf3e89', 'Test', 'Peter', '20000402', 'm', '200');
INSERT INTO `characters` VALUES (4, 'steam:11000011300960a', 'Asdef', 'Aysdfc', '20000000', 'm', '200');
INSERT INTO `characters` VALUES (5, 'steam:11000011300960a', 'Lukas', 'Jac', '2000-02-27', 'm', '200');

-- ----------------------------
-- Table structure for datastore
-- ----------------------------
DROP TABLE IF EXISTS `datastore`;
CREATE TABLE `datastore`  (
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `shared` int(0) NOT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of datastore
-- ----------------------------
INSERT INTO `datastore` VALUES ('property', 'Propriété', 0);
INSERT INTO `datastore` VALUES ('society_ambulance', 'Ambulance', 1);
INSERT INTO `datastore` VALUES ('society_police', 'Polizei', 1);
INSERT INTO `datastore` VALUES ('user_ears', 'Ears', 0);
INSERT INTO `datastore` VALUES ('user_glasses', 'Glasses', 0);
INSERT INTO `datastore` VALUES ('user_helmet', 'Helmet', 0);
INSERT INTO `datastore` VALUES ('user_mask', 'Mask', 0);

-- ----------------------------
-- Table structure for datastore_data
-- ----------------------------
DROP TABLE IF EXISTS `datastore_data`;
CREATE TABLE `datastore_data`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `owner` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_datastore_data_name_owner`(`name`, `owner`) USING BTREE,
  INDEX `index_datastore_data_name`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of datastore_data
-- ----------------------------
INSERT INTO `datastore_data` VALUES (1, 'user_ears', 'steam:110000114cf3e89', '{}');
INSERT INTO `datastore_data` VALUES (2, 'user_glasses', 'steam:110000114cf3e89', '{}');
INSERT INTO `datastore_data` VALUES (3, 'user_helmet', 'steam:110000114cf3e89', '{}');
INSERT INTO `datastore_data` VALUES (4, 'user_mask', 'steam:110000114cf3e89', '{}');
INSERT INTO `datastore_data` VALUES (5, 'user_glasses', 'steam:11000011300960a', '{}');
INSERT INTO `datastore_data` VALUES (6, 'user_ears', 'steam:11000011300960a', '{}');
INSERT INTO `datastore_data` VALUES (7, 'user_mask', 'steam:11000011300960a', '{}');
INSERT INTO `datastore_data` VALUES (8, 'user_helmet', 'steam:11000011300960a', '{}');
INSERT INTO `datastore_data` VALUES (9, 'society_police', NULL, '{}');
INSERT INTO `datastore_data` VALUES (10, 'society_ambulance', NULL, '{}');
INSERT INTO `datastore_data` VALUES (11, 'property', 'steam:110000114cf3e89', '{}');
INSERT INTO `datastore_data` VALUES (12, 'property', 'steam:11000011300960a', '{}');

-- ----------------------------
-- Table structure for fine_types
-- ----------------------------
DROP TABLE IF EXISTS `fine_types`;
CREATE TABLE `fine_types`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `amount` int(0) NULL DEFAULT NULL,
  `category` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fine_types
-- ----------------------------
INSERT INTO `fine_types` VALUES (1, 'Missbrauchen der Hupe', 30, 0);
INSERT INTO `fine_types` VALUES (2, 'Vorfahrtstraße missachtet', 40, 0);
INSERT INTO `fine_types` VALUES (3, 'Gegenverkehr', 250, 0);
INSERT INTO `fine_types` VALUES (4, 'Unbefugtes Wenden', 250, 0);
INSERT INTO `fine_types` VALUES (5, 'Off-Road fahren', 170, 0);
INSERT INTO `fine_types` VALUES (6, 'Sicherheitsrichtlinien missachtet', 30, 0);
INSERT INTO `fine_types` VALUES (7, 'Gefährliches halten', 150, 0);
INSERT INTO `fine_types` VALUES (8, 'Nicht erlaubtes parken', 70, 0);
INSERT INTO `fine_types` VALUES (9, 'Nicht respektieren des Rechtsfahrgebotes', 70, 0);
INSERT INTO `fine_types` VALUES (10, 'Nicht einhaltender Priorität eines Rettungsfahrzeuges', 90, 0);
INSERT INTO `fine_types` VALUES (11, 'Nicht angehalten nach Aufforderung', 105, 0);
INSERT INTO `fine_types` VALUES (12, 'Bei Rot über die Ampel', 130, 0);
INSERT INTO `fine_types` VALUES (13, 'gefährliches Überholmanöver', 100, 0);
INSERT INTO `fine_types` VALUES (14, 'Fahrzeug nicht Verkehrstauglich', 100, 0);
INSERT INTO `fine_types` VALUES (15, 'Fahren ohne Lizenz', 1500, 0);
INSERT INTO `fine_types` VALUES (16, 'Hit and Run', 800, 0);
INSERT INTO `fine_types` VALUES (17, 'zu schnell < 5 kmh', 90, 0);
INSERT INTO `fine_types` VALUES (18, 'zu schnell 5-15 kmh', 120, 0);
INSERT INTO `fine_types` VALUES (19, 'zu schnell 15-30 kmh', 180, 0);
INSERT INTO `fine_types` VALUES (20, 'zu schnell > 30 kmh', 300, 0);
INSERT INTO `fine_types` VALUES (21, 'Verkehrsbehinderung', 110, 1);
INSERT INTO `fine_types` VALUES (22, 'Nicht Benutzung öffentlicher Straßen', 90, 1);
INSERT INTO `fine_types` VALUES (23, 'Störung der öffentlichen Ordnung', 90, 1);
INSERT INTO `fine_types` VALUES (24, 'Polizeieinsatz behindern', 130, 1);
INSERT INTO `fine_types` VALUES (25, 'Beildigung an Zivilist', 75, 1);
INSERT INTO `fine_types` VALUES (26, 'Polizist geringschätzen', 110, 1);
INSERT INTO `fine_types` VALUES (27, 'verbale Bedrohung an Zivilist', 90, 1);
INSERT INTO `fine_types` VALUES (28, 'verbale Bedrohung an Polizist', 150, 1);
INSERT INTO `fine_types` VALUES (29, 'Illegale Demonstration', 250, 1);
INSERT INTO `fine_types` VALUES (30, 'versuchte Bestechung', 1500, 1);
INSERT INTO `fine_types` VALUES (31, 'Mit Messer in der Stadt bewaffnet', 120, 2);
INSERT INTO `fine_types` VALUES (32, 'Mit tötlicher Waffe bewaffnet', 300, 2);
INSERT INTO `fine_types` VALUES (33, 'Unbefugter Besitz einer Waffe (Standard-Lizenz)', 600, 2);
INSERT INTO `fine_types` VALUES (34, 'Illegale Waffe', 700, 2);
INSERT INTO `fine_types` VALUES (35, 'Einbruch/Aufbruch', 300, 2);
INSERT INTO `fine_types` VALUES (36, 'Autodiebstahl', 1800, 2);
INSERT INTO `fine_types` VALUES (37, 'Drogenverkauf', 1500, 2);
INSERT INTO `fine_types` VALUES (38, 'Drogen herstellen', 1500, 2);
INSERT INTO `fine_types` VALUES (39, 'Drogenbesitz', 650, 2);
INSERT INTO `fine_types` VALUES (40, 'Zivile Geisel genommen', 1500, 2);
INSERT INTO `fine_types` VALUES (41, 'Geiselnahme', 2000, 2);
INSERT INTO `fine_types` VALUES (42, 'Waghalsiges Lenken', 650, 2);
INSERT INTO `fine_types` VALUES (43, 'Ladenraub', 650, 2);
INSERT INTO `fine_types` VALUES (44, 'Bankraub', 1500, 2);
INSERT INTO `fine_types` VALUES (45, 'Schießen auf Zivilisten', 2000, 3);
INSERT INTO `fine_types` VALUES (46, 'Schießen auf Beamten', 2500, 3);
INSERT INTO `fine_types` VALUES (47, 'versuchterMord an Zivilisten', 3000, 3);
INSERT INTO `fine_types` VALUES (48, 'versuchter Mord an einem Beamten', 5000, 3);
INSERT INTO `fine_types` VALUES (49, 'Mord an Zivilisten', 10000, 3);
INSERT INTO `fine_types` VALUES (50, 'Mord an Beamten', 30000, 3);
INSERT INTO `fine_types` VALUES (51, 'versehentlicher Mord', 1800, 3);
INSERT INTO `fine_types` VALUES (52, 'Firmenbetrug', 2000, 2);

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items`  (
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `label` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `weight` int(0) NOT NULL DEFAULT 1,
  `rare` tinyint(1) NOT NULL DEFAULT 0,
  `can_remove` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of items
-- ----------------------------
INSERT INTO `items` VALUES ('bandage', 'Bandage', 2, 0, 1);
INSERT INTO `items` VALUES ('bread', 'Brot', 1, 0, 1);
INSERT INTO `items` VALUES ('medikit', 'Medikit', 2, 0, 1);
INSERT INTO `items` VALUES ('water', 'Wasser', 1, 0, 1);

-- ----------------------------
-- Table structure for job_grades
-- ----------------------------
DROP TABLE IF EXISTS `job_grades`;
CREATE TABLE `job_grades`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `job_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `grade` int(0) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `label` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `salary` int(0) NOT NULL,
  `skin_male` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `skin_female` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of job_grades
-- ----------------------------
INSERT INTO `job_grades` VALUES (1, 'unemployed', 0, 'unemployed', 'Unemployed', 200, '{}', '{}');
INSERT INTO `job_grades` VALUES (2, 'cardealer', 0, 'recruit', 'Neuling', 10, '{}', '{}');
INSERT INTO `job_grades` VALUES (3, 'cardealer', 1, 'novice', 'Verkäufer', 25, '{}', '{}');
INSERT INTO `job_grades` VALUES (4, 'cardealer', 2, 'experienced', 'Verkaufsleiter', 40, '{}', '{}');
INSERT INTO `job_grades` VALUES (5, 'cardealer', 3, 'boss', 'Boss', 0, '{}', '{}');
INSERT INTO `job_grades` VALUES (6, 'police', 0, 'recruit', 'Rekrut', 20, '{}', '{}');
INSERT INTO `job_grades` VALUES (7, 'police', 1, 'officer', 'Officier', 40, '{}', '{}');
INSERT INTO `job_grades` VALUES (8, 'police', 2, 'sergeant', 'Sergent', 60, '{}', '{}');
INSERT INTO `job_grades` VALUES (9, 'police', 3, 'lieutenant', 'Lieutenant', 85, '{}', '{}');
INSERT INTO `job_grades` VALUES (10, 'police', 4, 'boss', 'Commandant', 100, '{}', '{}');
INSERT INTO `job_grades` VALUES (11, 'ambulance', 0, 'ambulance', 'Ambulancier', 20, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}');
INSERT INTO `job_grades` VALUES (12, 'ambulance', 1, 'doctor', 'Medecin', 40, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}');
INSERT INTO `job_grades` VALUES (13, 'ambulance', 2, 'chief_doctor', 'Medecin-chef', 60, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}');
INSERT INTO `job_grades` VALUES (14, 'ambulance', 3, 'boss', 'Chirurgien', 80, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}');

-- ----------------------------
-- Table structure for jobs
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs`  (
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `label` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jobs
-- ----------------------------
INSERT INTO `jobs` VALUES ('ambulance', 'Ambulance');
INSERT INTO `jobs` VALUES ('cardealer', 'Händler');
INSERT INTO `jobs` VALUES ('police', 'Polizei');
INSERT INTO `jobs` VALUES ('unemployed', 'Unemployed');

-- ----------------------------
-- Table structure for licenses
-- ----------------------------
DROP TABLE IF EXISTS `licenses`;
CREATE TABLE `licenses`  (
  `type` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `label` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`type`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for owned_properties
-- ----------------------------
DROP TABLE IF EXISTS `owned_properties`;
CREATE TABLE `owned_properties`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `price` double NOT NULL,
  `rented` int(0) NOT NULL,
  `owner` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for owned_vehicles
-- ----------------------------
DROP TABLE IF EXISTS `owned_vehicles`;
CREATE TABLE `owned_vehicles`  (
  `owner` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `plate` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `vehicle` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'car',
  `job` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `stored` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`plate`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for phone_app_chat
-- ----------------------------
DROP TABLE IF EXISTS `phone_app_chat`;
CREATE TABLE `phone_app_chat`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `channel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for phone_calls
-- ----------------------------
DROP TABLE IF EXISTS `phone_calls`;
CREATE TABLE `phone_calls`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `owner` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Num tel proprio',
  `num` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Num reférence du contact',
  `incoming` int(0) NOT NULL COMMENT 'Défini si on est à l\'origine de l\'appels',
  `time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `accepts` int(0) NOT NULL COMMENT 'Appels accepter ou pas',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 122 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of phone_calls
-- ----------------------------
INSERT INTO `phone_calls` VALUES (122, '827-5849', '906-8228', 1, '2020-02-15 22:55:14', 1);
INSERT INTO `phone_calls` VALUES (123, '906-8228', '827-5849', 0, '2020-02-15 22:55:14', 1);
INSERT INTO `phone_calls` VALUES (124, '827-5849', '906-8228', 1, '2020-02-15 22:55:17', 1);
INSERT INTO `phone_calls` VALUES (125, '906-8228', '827-5849', 0, '2020-02-15 22:55:17', 1);
INSERT INTO `phone_calls` VALUES (126, '827-5849', '906-8228', 1, '2020-02-15 22:55:17', 1);
INSERT INTO `phone_calls` VALUES (127, '906-8228', '827-5849', 0, '2020-02-15 22:55:17', 1);
INSERT INTO `phone_calls` VALUES (128, '827-5849', '906-8228', 1, '2020-02-15 22:56:36', 1);
INSERT INTO `phone_calls` VALUES (129, '906-8228', '827-5849', 0, '2020-02-15 22:56:36', 1);
INSERT INTO `phone_calls` VALUES (130, '827-5849', '906-8228', 1, '2020-02-15 22:56:37', 1);
INSERT INTO `phone_calls` VALUES (131, '906-8228', '827-5849', 0, '2020-02-15 22:56:37', 1);
INSERT INTO `phone_calls` VALUES (132, '827-5849', '906-8228', 1, '2020-02-15 23:01:00', 1);
INSERT INTO `phone_calls` VALUES (133, '906-8228', '827-5849', 0, '2020-02-15 23:01:00', 1);
INSERT INTO `phone_calls` VALUES (134, '827-5849', '906-8228', 1, '2020-02-15 23:02:19', 1);
INSERT INTO `phone_calls` VALUES (135, '906-8228', '827-5849', 0, '2020-02-15 23:02:19', 1);
INSERT INTO `phone_calls` VALUES (136, '827-5849', '906-8228', 1, '2020-02-15 23:07:55', 0);
INSERT INTO `phone_calls` VALUES (137, '906-8228', '827-5849', 0, '2020-02-15 23:07:55', 0);
INSERT INTO `phone_calls` VALUES (138, '827-5849', '906-8228', 1, '2020-02-15 23:10:02', 0);
INSERT INTO `phone_calls` VALUES (139, '906-8228', '827-5849', 0, '2020-02-15 23:10:02', 0);
INSERT INTO `phone_calls` VALUES (140, '827-5849', '906-8228', 1, '2020-02-15 23:11:05', 1);
INSERT INTO `phone_calls` VALUES (141, '906-8228', '827-5849', 0, '2020-02-15 23:11:05', 1);
INSERT INTO `phone_calls` VALUES (142, '827-5849', '906-8228', 1, '2020-02-15 23:18:04', 1);
INSERT INTO `phone_calls` VALUES (143, '906-8228', '827-5849', 0, '2020-02-15 23:18:04', 1);
INSERT INTO `phone_calls` VALUES (144, '906-8228', '827-5849', 1, '2020-02-15 23:18:17', 1);
INSERT INTO `phone_calls` VALUES (145, '827-5849', '906-8228', 0, '2020-02-15 23:18:17', 1);
INSERT INTO `phone_calls` VALUES (146, '906-8228', '827-5849', 0, '2020-02-15 23:23:58', 1);
INSERT INTO `phone_calls` VALUES (147, '827-5849', '906-8228', 1, '2020-02-15 23:23:58', 1);
INSERT INTO `phone_calls` VALUES (148, '906-8228', '827-5849', 0, '2020-02-15 23:30:26', 1);
INSERT INTO `phone_calls` VALUES (149, '827-5849', '906-8228', 1, '2020-02-15 23:30:26', 1);
INSERT INTO `phone_calls` VALUES (150, '827-5849', '906-8228', 1, '2020-02-15 23:30:34', 1);
INSERT INTO `phone_calls` VALUES (151, '906-8228', '827-5849', 0, '2020-02-15 23:30:34', 1);
INSERT INTO `phone_calls` VALUES (152, '906-8228', '827-5849', 1, '2020-02-15 23:32:54', 0);
INSERT INTO `phone_calls` VALUES (153, '827-5849', '906-8228', 0, '2020-02-15 23:32:54', 0);
INSERT INTO `phone_calls` VALUES (154, '906-8228', '827-5849', 1, '2020-02-15 23:33:17', 1);
INSERT INTO `phone_calls` VALUES (155, '827-5849', '906-8228', 0, '2020-02-15 23:33:17', 1);
INSERT INTO `phone_calls` VALUES (156, '827-5849', '906-8228', 1, '2020-02-15 23:33:21', 1);
INSERT INTO `phone_calls` VALUES (157, '906-8228', '827-5849', 0, '2020-02-15 23:33:21', 1);
INSERT INTO `phone_calls` VALUES (158, '906-8228', '827-5849', 0, '2020-02-16 10:14:16', 0);
INSERT INTO `phone_calls` VALUES (159, '827-5849', '906-8228', 1, '2020-02-16 10:14:16', 0);

-- ----------------------------
-- Table structure for phone_messages
-- ----------------------------
DROP TABLE IF EXISTS `phone_messages`;
CREATE TABLE `phone_messages`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `transmitter` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `receiver` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `isRead` int(0) NOT NULL DEFAULT 0,
  `owner` int(0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 106 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for phone_users_contacts
-- ----------------------------
DROP TABLE IF EXISTS `phone_users_contacts`;
CREATE TABLE `phone_users_contacts`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `number` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `display` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for properties
-- ----------------------------
DROP TABLE IF EXISTS `properties`;
CREATE TABLE `properties`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `entering` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `exit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `inside` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `outside` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ipls` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '[]',
  `gateway` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `is_single` int(0) NULL DEFAULT NULL,
  `is_room` int(0) NULL DEFAULT NULL,
  `is_gateway` int(0) NULL DEFAULT NULL,
  `room_menu` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `price` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of properties
-- ----------------------------
INSERT INTO `properties` VALUES (1, 'WhispymoundDrive', '2677 Whispymound Drive', '{\"y\":564.89,\"z\":182.959,\"x\":119.384}', '{\"x\":117.347,\"y\":559.506,\"z\":183.304}', '{\"y\":557.032,\"z\":183.301,\"x\":118.037}', '{\"y\":567.798,\"z\":182.131,\"x\":119.249}', '[]', NULL, 1, 1, 0, '{\"x\":118.748,\"y\":566.573,\"z\":175.697}', 1500000);
INSERT INTO `properties` VALUES (2, 'NorthConkerAvenue2045', '2045 North Conker Avenue', '{\"x\":372.796,\"y\":428.327,\"z\":144.685}', '{\"x\":373.548,\"y\":422.982,\"z\":144.907},', '{\"y\":420.075,\"z\":145.904,\"x\":372.161}', '{\"x\":372.454,\"y\":432.886,\"z\":143.443}', '[]', NULL, 1, 1, 0, '{\"x\":377.349,\"y\":429.422,\"z\":137.3}', 1500000);
INSERT INTO `properties` VALUES (3, 'RichardMajesticApt2', 'Richard Majestic, Apt 2', '{\"y\":-379.165,\"z\":37.961,\"x\":-936.363}', '{\"y\":-365.476,\"z\":113.274,\"x\":-913.097}', '{\"y\":-367.637,\"z\":113.274,\"x\":-918.022}', '{\"y\":-382.023,\"z\":37.961,\"x\":-943.626}', '[]', NULL, 1, 1, 0, '{\"x\":-927.554,\"y\":-377.744,\"z\":112.674}', 1700000);
INSERT INTO `properties` VALUES (4, 'NorthConkerAvenue2044', '2044 North Conker Avenue', '{\"y\":440.8,\"z\":146.702,\"x\":346.964}', '{\"y\":437.456,\"z\":148.394,\"x\":341.683}', '{\"y\":435.626,\"z\":148.394,\"x\":339.595}', '{\"x\":350.535,\"y\":443.329,\"z\":145.764}', '[]', NULL, 1, 1, 0, '{\"x\":337.726,\"y\":436.985,\"z\":140.77}', 1500000);
INSERT INTO `properties` VALUES (5, 'WildOatsDrive', '3655 Wild Oats Drive', '{\"y\":502.696,\"z\":136.421,\"x\":-176.003}', '{\"y\":497.817,\"z\":136.653,\"x\":-174.349}', '{\"y\":495.069,\"z\":136.666,\"x\":-173.331}', '{\"y\":506.412,\"z\":135.0664,\"x\":-177.927}', '[]', NULL, 1, 1, 0, '{\"x\":-174.725,\"y\":493.095,\"z\":129.043}', 1500000);
INSERT INTO `properties` VALUES (6, 'HillcrestAvenue2862', '2862 Hillcrest Avenue', '{\"y\":596.58,\"z\":142.641,\"x\":-686.554}', '{\"y\":591.988,\"z\":144.392,\"x\":-681.728}', '{\"y\":590.608,\"z\":144.392,\"x\":-680.124}', '{\"y\":599.019,\"z\":142.059,\"x\":-689.492}', '[]', NULL, 1, 1, 0, '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', 1500000);
INSERT INTO `properties` VALUES (7, 'LowEndApartment', 'Appartement de base', '{\"y\":-1078.735,\"z\":28.4031,\"x\":292.528}', '{\"y\":-1007.152,\"z\":-102.002,\"x\":265.845}', '{\"y\":-1002.802,\"z\":-100.008,\"x\":265.307}', '{\"y\":-1078.669,\"z\":28.401,\"x\":296.738}', '[]', NULL, 1, 1, 0, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', 562500);
INSERT INTO `properties` VALUES (8, 'MadWayneThunder', '2113 Mad Wayne Thunder', '{\"y\":454.955,\"z\":96.462,\"x\":-1294.433}', '{\"x\":-1289.917,\"y\":449.541,\"z\":96.902}', '{\"y\":446.322,\"z\":96.899,\"x\":-1289.642}', '{\"y\":455.453,\"z\":96.517,\"x\":-1298.851}', '[]', NULL, 1, 1, 0, '{\"x\":-1287.306,\"y\":455.901,\"z\":89.294}', 1500000);
INSERT INTO `properties` VALUES (9, 'HillcrestAvenue2874', '2874 Hillcrest Avenue', '{\"x\":-853.346,\"y\":696.678,\"z\":147.782}', '{\"y\":690.875,\"z\":151.86,\"x\":-859.961}', '{\"y\":688.361,\"z\":151.857,\"x\":-859.395}', '{\"y\":701.628,\"z\":147.773,\"x\":-855.007}', '[]', NULL, 1, 1, 0, '{\"x\":-858.543,\"y\":697.514,\"z\":144.253}', 1500000);
INSERT INTO `properties` VALUES (10, 'HillcrestAvenue2868', '2868 Hillcrest Avenue', '{\"y\":620.494,\"z\":141.588,\"x\":-752.82}', '{\"y\":618.62,\"z\":143.153,\"x\":-759.317}', '{\"y\":617.629,\"z\":143.153,\"x\":-760.789}', '{\"y\":621.281,\"z\":141.254,\"x\":-750.919}', '[]', NULL, 1, 1, 0, '{\"x\":-762.504,\"y\":618.992,\"z\":135.53}', 1500000);
INSERT INTO `properties` VALUES (11, 'TinselTowersApt12', 'Tinsel Towers, Apt 42', '{\"y\":37.025,\"z\":42.58,\"x\":-618.299}', '{\"y\":58.898,\"z\":97.2,\"x\":-603.301}', '{\"y\":58.941,\"z\":97.2,\"x\":-608.741}', '{\"y\":30.603,\"z\":42.524,\"x\":-620.017}', '[]', NULL, 1, 1, 0, '{\"x\":-622.173,\"y\":54.585,\"z\":96.599}', 1700000);
INSERT INTO `properties` VALUES (12, 'MiltonDrive', 'Milton Drive', '{\"x\":-775.17,\"y\":312.01,\"z\":84.658}', NULL, NULL, '{\"x\":-775.346,\"y\":306.776,\"z\":84.7}', '[]', NULL, 0, 0, 1, NULL, 0);
INSERT INTO `properties` VALUES (13, 'Modern1Apartment', 'Appartement Moderne 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_01_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.661,\"y\":327.672,\"z\":210.396}', 1300000);
INSERT INTO `properties` VALUES (14, 'Modern2Apartment', 'Appartement Moderne 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_01_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.735,\"y\":326.757,\"z\":186.313}', 1300000);
INSERT INTO `properties` VALUES (15, 'Modern3Apartment', 'Appartement Moderne 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_01_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.386,\"y\":330.782,\"z\":195.08}', 1300000);
INSERT INTO `properties` VALUES (16, 'Mody1Apartment', 'Appartement Mode 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_02_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.615,\"y\":327.878,\"z\":210.396}', 1300000);
INSERT INTO `properties` VALUES (17, 'Mody2Apartment', 'Appartement Mode 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_02_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.297,\"y\":327.092,\"z\":186.313}', 1300000);
INSERT INTO `properties` VALUES (18, 'Mody3Apartment', 'Appartement Mode 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_02_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.303,\"y\":330.932,\"z\":195.085}', 1300000);
INSERT INTO `properties` VALUES (19, 'Vibrant1Apartment', 'Appartement Vibrant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_03_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.885,\"y\":327.641,\"z\":210.396}', 1300000);
INSERT INTO `properties` VALUES (20, 'Vibrant2Apartment', 'Appartement Vibrant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_03_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.607,\"y\":327.344,\"z\":186.313}', 1300000);
INSERT INTO `properties` VALUES (21, 'Vibrant3Apartment', 'Appartement Vibrant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_03_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.525,\"y\":330.851,\"z\":195.085}', 1300000);
INSERT INTO `properties` VALUES (22, 'Sharp1Apartment', 'Appartement Persan 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_04_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.527,\"y\":327.89,\"z\":210.396}', 1300000);
INSERT INTO `properties` VALUES (23, 'Sharp2Apartment', 'Appartement Persan 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_04_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.642,\"y\":326.497,\"z\":186.313}', 1300000);
INSERT INTO `properties` VALUES (24, 'Sharp3Apartment', 'Appartement Persan 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_04_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.503,\"y\":331.318,\"z\":195.085}', 1300000);
INSERT INTO `properties` VALUES (25, 'Monochrome1Apartment', 'Appartement Monochrome 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_05_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.289,\"y\":328.086,\"z\":210.396}', 1300000);
INSERT INTO `properties` VALUES (26, 'Monochrome2Apartment', 'Appartement Monochrome 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_05_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.692,\"y\":326.762,\"z\":186.313}', 1300000);
INSERT INTO `properties` VALUES (27, 'Monochrome3Apartment', 'Appartement Monochrome 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_05_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.094,\"y\":330.976,\"z\":195.085}', 1300000);
INSERT INTO `properties` VALUES (28, 'Seductive1Apartment', 'Appartement Séduisant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_06_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.263,\"y\":328.104,\"z\":210.396}', 1300000);
INSERT INTO `properties` VALUES (29, 'Seductive2Apartment', 'Appartement Séduisant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_06_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.655,\"y\":326.611,\"z\":186.313}', 1300000);
INSERT INTO `properties` VALUES (30, 'Seductive3Apartment', 'Appartement Séduisant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_06_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.3,\"y\":331.414,\"z\":195.085}', 1300000);
INSERT INTO `properties` VALUES (31, 'Regal1Apartment', 'Appartement Régal 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_07_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.956,\"y\":328.257,\"z\":210.396}', 1300000);
INSERT INTO `properties` VALUES (32, 'Regal2Apartment', 'Appartement Régal 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_07_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.545,\"y\":326.659,\"z\":186.313}', 1300000);
INSERT INTO `properties` VALUES (33, 'Regal3Apartment', 'Appartement Régal 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_07_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.087,\"y\":331.429,\"z\":195.123}', 1300000);
INSERT INTO `properties` VALUES (34, 'Aqua1Apartment', 'Appartement Aqua 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_08_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.187,\"y\":328.47,\"z\":210.396}', 1300000);
INSERT INTO `properties` VALUES (35, 'Aqua2Apartment', 'Appartement Aqua 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_08_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.658,\"y\":326.563,\"z\":186.313}', 1300000);
INSERT INTO `properties` VALUES (36, 'Aqua3Apartment', 'Appartement Aqua 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_08_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.287,\"y\":331.084,\"z\":195.086}', 1300000);
INSERT INTO `properties` VALUES (37, 'IntegrityWay', '4 Integrity Way', '{\"x\":-47.804,\"y\":-585.867,\"z\":36.956}', NULL, NULL, '{\"x\":-54.178,\"y\":-583.762,\"z\":35.798}', '[]', NULL, 0, 0, 1, NULL, 0);
INSERT INTO `properties` VALUES (38, 'IntegrityWay28', '4 Integrity Way - Apt 28', NULL, '{\"x\":-31.409,\"y\":-594.927,\"z\":79.03}', '{\"x\":-26.098,\"y\":-596.909,\"z\":79.03}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-11.923,\"y\":-597.083,\"z\":78.43}', 1700000);
INSERT INTO `properties` VALUES (39, 'IntegrityWay30', '4 Integrity Way - Apt 30', NULL, '{\"x\":-17.702,\"y\":-588.524,\"z\":89.114}', '{\"x\":-16.21,\"y\":-582.569,\"z\":89.114}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-26.327,\"y\":-588.384,\"z\":89.123}', 1700000);
INSERT INTO `properties` VALUES (40, 'DellPerroHeights', 'Dell Perro Heights', '{\"x\":-1447.06,\"y\":-538.28,\"z\":33.74}', NULL, NULL, '{\"x\":-1440.022,\"y\":-548.696,\"z\":33.74}', '[]', NULL, 0, 0, 1, NULL, 0);
INSERT INTO `properties` VALUES (41, 'DellPerroHeightst4', 'Dell Perro Heights - Apt 28', NULL, '{\"x\":-1452.125,\"y\":-540.591,\"z\":73.044}', '{\"x\":-1455.435,\"y\":-535.79,\"z\":73.044}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1467.058,\"y\":-527.571,\"z\":72.443}', 1700000);
INSERT INTO `properties` VALUES (42, 'DellPerroHeightst7', 'Dell Perro Heights - Apt 30', NULL, '{\"x\":-1451.562,\"y\":-523.535,\"z\":55.928}', '{\"x\":-1456.02,\"y\":-519.209,\"z\":55.929}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1457.026,\"y\":-530.219,\"z\":55.937}', 1700000);

-- ----------------------------
-- Table structure for rented_vehicles
-- ----------------------------
DROP TABLE IF EXISTS `rented_vehicles`;
CREATE TABLE `rented_vehicles`  (
  `vehicle` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `plate` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `player_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `base_price` int(0) NOT NULL,
  `rent_price` int(0) NOT NULL,
  `owner` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`plate`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shops
-- ----------------------------
DROP TABLE IF EXISTS `shops`;
CREATE TABLE `shops`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `store` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `item` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `price` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shops
-- ----------------------------
INSERT INTO `shops` VALUES (1, 'TwentyFourSeven', 'bread', 30);
INSERT INTO `shops` VALUES (2, 'TwentyFourSeven', 'water', 15);
INSERT INTO `shops` VALUES (3, 'RobsLiquor', 'bread', 30);
INSERT INTO `shops` VALUES (4, 'RobsLiquor', 'water', 15);
INSERT INTO `shops` VALUES (5, 'LTDgasoline', 'bread', 30);
INSERT INTO `shops` VALUES (6, 'LTDgasoline', 'water', 15);

-- ----------------------------
-- Table structure for society_moneywash
-- ----------------------------
DROP TABLE IF EXISTS `society_moneywash`;
CREATE TABLE `society_moneywash`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `society` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `amount` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_accounts
-- ----------------------------
DROP TABLE IF EXISTS `user_accounts`;
CREATE TABLE `user_accounts`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `money` int(0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_accounts
-- ----------------------------
INSERT INTO `user_accounts` VALUES (1, 'steam:110000114cf3e89', 'black_money', 0);
INSERT INTO `user_accounts` VALUES (2, 'steam:110000110b3ac66', 'black_money', 0);
INSERT INTO `user_accounts` VALUES (3, 'Char2:110000114cf3e89', 'black_money', 0);
INSERT INTO `user_accounts` VALUES (5, 'steam:11000011300960a', 'black_money', 0);

-- ----------------------------
-- Table structure for user_lastcharacter
-- ----------------------------
DROP TABLE IF EXISTS `user_lastcharacter`;
CREATE TABLE `user_lastcharacter`  (
  `steamid` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `charid` int(0) NOT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_lastcharacter
-- ----------------------------
INSERT INTO `user_lastcharacter` VALUES ('steam:110000114cf3e89', 1);
INSERT INTO `user_lastcharacter` VALUES ('steam:110000110b3ac66', 1);
INSERT INTO `user_lastcharacter` VALUES ('steam:11000011300960a', 2);

-- ----------------------------
-- Table structure for user_licenses
-- ----------------------------
DROP TABLE IF EXISTS `user_licenses`;
CREATE TABLE `user_licenses`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `type` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `owner` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `identifier` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `money` int(0) NULL DEFAULT NULL,
  `bank` int(0) NULL DEFAULT NULL,
  `permission_level` int(0) NULL DEFAULT NULL,
  `group` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `skin` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL,
  `job` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'unemployed',
  `job_grade` int(0) NULL DEFAULT 0,
  `loadout` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL,
  `inventory` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL,
  `position` varchar(53) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '{\"x\":-269.4,\"y\":-955.3,\"z\":31.2,\"heading\":205.8}',
  `firstname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '',
  `lastname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '',
  `dateofbirth` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '',
  `sex` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '',
  `height` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '',
  `phone_number` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `is_dead` tinyint(1) NULL DEFAULT 0,
  `last_property` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`identifier`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('steam:11000011300960a', 'license:b40f7e504e72fc272f41b142d87b033203f75cb6', 0, 2200, 0, 'user', '{\"torso_1\":0,\"tshirt_2\":0,\"bags_1\":0,\"torso_2\":0,\"makeup_3\":0,\"makeup_2\":0,\"helmet_1\":-1,\"bags_2\":0,\"decals_1\":0,\"pants_2\":0,\"blush_2\":0,\"beard_4\":0,\"eyebrows_3\":0,\"sex\":0,\"lipstick_1\":0,\"blemishes_1\":0,\"watches_1\":-1,\"chest_2\":0,\"mask_2\":0,\"arms_2\":0,\"pants_1\":0,\"mask_1\":0,\"makeup_4\":0,\"age_2\":0,\"sun_1\":0,\"moles_2\":0,\"ears_1\":-1,\"bracelets_2\":0,\"eyebrows_4\":0,\"bodyb_2\":0,\"tshirt_1\":0,\"bracelets_1\":-1,\"watches_2\":0,\"chain_1\":0,\"makeup_1\":0,\"chest_1\":0,\"chain_2\":0,\"decals_2\":0,\"beard_2\":0,\"skin\":0,\"complexion_1\":0,\"bodyb_1\":0,\"hair_color_2\":0,\"bproof_1\":0,\"age_1\":0,\"complexion_2\":0,\"hair_1\":0,\"lipstick_2\":0,\"arms\":0,\"beard_1\":0,\"lipstick_3\":0,\"sun_2\":0,\"helmet_2\":0,\"blemishes_2\":0,\"chest_3\":0,\"hair_2\":0,\"hair_color_1\":0,\"eye_color\":0,\"moles_1\":0,\"shoes_1\":0,\"ears_2\":0,\"glasses_2\":0,\"glasses_1\":0,\"face\":0,\"beard_3\":0,\"shoes_2\":0,\"bproof_2\":0,\"eyebrows_1\":0,\"eyebrows_2\":0,\"blush_1\":0,\"lipstick_4\":0,\"blush_3\":0}', 'unemployed', 0, '[]', '[]', '{\"y\":-772.4,\"z\":44.1,\"heading\":64.3,\"x\":-6.4}', 'Lukas', 'Jac', '2000-02-27', 'm', '200', '906-8228', 0, NULL);
INSERT INTO `users` VALUES ('steam:110000114cf3e89', 'license:5589864dee66a5517bd42a049f8c2afa4bc02bb1', 0, 3400, 0, 'user', '{\"arms\":0,\"bags_1\":0,\"watches_2\":0,\"chest_2\":0,\"mask_2\":0,\"chest_1\":0,\"sun_1\":0,\"lipstick_3\":0,\"helmet_2\":0,\"blush_2\":0,\"sun_2\":0,\"decals_1\":0,\"arms_2\":0,\"eyebrows_2\":0,\"decals_2\":0,\"eyebrows_3\":0,\"glasses_1\":0,\"complexion_1\":0,\"tshirt_1\":0,\"makeup_3\":0,\"bracelets_2\":0,\"eye_color\":0,\"blush_3\":0,\"moles_1\":0,\"blemishes_1\":0,\"blush_1\":0,\"ears_2\":0,\"lipstick_2\":0,\"torso_1\":0,\"bracelets_1\":-1,\"shoes_2\":0,\"torso_2\":0,\"hair_2\":0,\"hair_color_2\":0,\"makeup_4\":0,\"beard_4\":0,\"mask_1\":0,\"bodyb_1\":0,\"bproof_1\":0,\"beard_1\":0,\"beard_2\":0,\"age_1\":0,\"bags_2\":0,\"tshirt_2\":0,\"face\":0,\"eyebrows_4\":0,\"makeup_1\":0,\"glasses_2\":0,\"bproof_2\":0,\"chest_3\":0,\"complexion_2\":0,\"chain_2\":0,\"hair_1\":0,\"pants_1\":0,\"shoes_1\":0,\"bodyb_2\":0,\"pants_2\":0,\"skin\":0,\"watches_1\":-1,\"lipstick_1\":0,\"makeup_2\":0,\"helmet_1\":-1,\"ears_1\":-1,\"sex\":1,\"eyebrows_1\":0,\"lipstick_4\":0,\"hair_color_1\":0,\"moles_2\":0,\"beard_3\":0,\"chain_1\":0,\"blemishes_2\":0,\"age_2\":0}', 'unemployed', 0, '[]', '[]', '{\"y\":-1147.5,\"z\":28.8,\"heading\":76.0,\"x\":24.5}', 'Test', 'Peter', '20000402', 'm', '200', '827-5849', 0, NULL);

-- ----------------------------
-- Table structure for vehicle_categories
-- ----------------------------
DROP TABLE IF EXISTS `vehicle_categories`;
CREATE TABLE `vehicle_categories`  (
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `label` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of vehicle_categories
-- ----------------------------
INSERT INTO `vehicle_categories` VALUES ('audi', 'AUDI');
INSERT INTO `vehicle_categories` VALUES ('bmw', 'BMW');
INSERT INTO `vehicle_categories` VALUES ('dodge', 'Dodge');
INSERT INTO `vehicle_categories` VALUES ('ford', 'Ford');
INSERT INTO `vehicle_categories` VALUES ('mercedes', 'Mercedes');

-- ----------------------------
-- Table structure for vehicle_sold
-- ----------------------------
DROP TABLE IF EXISTS `vehicle_sold`;
CREATE TABLE `vehicle_sold`  (
  `client` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `model` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `plate` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `soldby` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `date` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`plate`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for vehicles
-- ----------------------------
DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles`  (
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `model` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `price` int(0) NOT NULL,
  `category` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`model`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of vehicles
-- ----------------------------
INSERT INTO `vehicles` VALUES ('E Klasse', 'e63amg', 320000, 'mercedes');
INSERT INTO `vehicles` VALUES ('Z4GT3', 'e89', 130000, 'bmw');
INSERT INTO `vehicles` VALUES ('G Klasse', 'g65amg', 250000, 'mercedes');
INSERT INTO `vehicles` VALUES ('Mustang', 'gt350r', 160000, 'ford');
INSERT INTO `vehicles` VALUES ('Challenger', 'hellcat', 210000, 'dodge');
INSERT INTO `vehicles` VALUES ('RS7', 'rmodrs7', 360000, 'audi');

SET FOREIGN_KEY_CHECKS = 1;
